package com.altimetrik.playground.gitlab.bean;

import java.util.HashSet;

public class AddMemberRequestBean {

	private Long projectId;
	private HashSet<MemberBean> memberSet = new HashSet<>();
	public Long getProjectDetailId() {
		return projectId;
	}
	public void setProjectDetailId(Long projectDetailId) {
		this.projectId = projectDetailId;
	}
	
	public HashSet<MemberBean> getMemberSet() {
		return memberSet;
	}
	public void setMemberSet(HashSet<MemberBean> memberSet) {
		this.memberSet = memberSet;
	}
	@Override
	public String toString() {
		return "AddMemberRequestBean [projectId=" + projectId + ", memberSet=" + memberSet + "]";
	}
	
	
}
