/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

import java.util.Arrays;

/**
 * @author nmuthusamy
 *
 */
public class GitCommitRequestBean {

	private String id;
	private String branch;
	private String commit_message;
	private GitCommitActionBean[] actions;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}
	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}
	/**
	 * @return the commit_message
	 */
	public String getCommit_message() {
		return commit_message;
	}
	/**
	 * @param commit_message the commit_message to set
	 */
	public void setCommit_message(String commit_message) {
		this.commit_message = commit_message;
	}
	/**
	 * @return the actions
	 */
	public GitCommitActionBean[] getActions() {
		return actions;
	}
	/**
	 * @param actions the actions to set
	 */
	public void setActions(GitCommitActionBean[] actions) {
		this.actions = actions;
	}
	@Override
	public String toString() {
		return "GitCommitRequestBean [id=" + id + ", branch=" + branch + ", commit_message=" + commit_message
				+ ", actions=" + Arrays.toString(actions) + "]";
	}
	
	
	
}
