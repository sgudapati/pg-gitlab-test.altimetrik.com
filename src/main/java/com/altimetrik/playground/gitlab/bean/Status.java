package com.altimetrik.playground.gitlab.bean;

public class Status {

	private Integer statusCode;
	private String messageDescription;
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageDescription() {
		return messageDescription;
	}
	public void setMessageDescription(String messageDescription) {
		this.messageDescription = messageDescription;
	}
	
	@Override
	public String toString() {
		return "Status [statusCode=" + statusCode + ", messageDescription=" + messageDescription + "]";
	}
	
}
