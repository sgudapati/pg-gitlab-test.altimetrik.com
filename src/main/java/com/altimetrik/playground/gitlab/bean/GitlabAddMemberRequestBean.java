package com.altimetrik.playground.gitlab.bean;

public class GitlabAddMemberRequestBean {

	private long id;
	private long user_id;
	private int access_level;

	
	
	public GitlabAddMemberRequestBean() {
		super();
	}

	public GitlabAddMemberRequestBean(long id, long user_id, int access_level) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.access_level = access_level;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public int getAccess_level() {
		return access_level;
	}

	public void setAccess_level(int access_level) {
		this.access_level = access_level;
	}

	@Override
	public String toString() {
		return "GitlabAddMemberRequestBean [id=" + id + ", user_id=" + user_id + ", access_level=" + access_level + "]";
	}

}
