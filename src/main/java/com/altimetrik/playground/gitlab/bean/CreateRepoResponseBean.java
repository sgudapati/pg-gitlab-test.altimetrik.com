/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 *
 */
public class CreateRepoResponseBean {

	private String name;
	private Long id;
	private String http_url_to_repo;
	private String description;
	private String ssh_url_to_repo;
	private String web_url;
	
	public CreateRepoResponseBean() {
		super();
	}
	public CreateRepoResponseBean(String name, Long id, String http_url_to_repo, String description, String ssh_url_to_repo,
			String web_url) {
		super();
		this.name = name;
		this.id = id;
		this.http_url_to_repo = http_url_to_repo;
		this.description = description;
		this.ssh_url_to_repo = ssh_url_to_repo;
		this.web_url = web_url;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getHttp_url_to_repo() {
		return http_url_to_repo;
	}
	public void setHttp_url_to_repo(String http_url_to_repo) {
		this.http_url_to_repo = http_url_to_repo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSsh_url_to_repo() {
		return ssh_url_to_repo;
	}
	public void setSsh_url_to_repo(String ssh_url_to_repo) {
		this.ssh_url_to_repo = ssh_url_to_repo;
	}
	public String getWeb_url() {
		return web_url;
	}
	public void setWeb_url(String web_url) {
		this.web_url = web_url;
	}
	
}
