/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

import java.util.List;

import com.altimetrik.playground.gitlab.entity.InterfaceOption;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.util.UniqueDateFormatter;

/**
 * @author nmuthusamy
 * created on 12-Oct-2018
 */
public class ProjectBean {

	private Long projectDetailId;

	private String requesterEmailId;
	
	private String createdBy;

	private String createdTime;

	private List<InterfaceOption> interfaceId;

	private String projectName;

	private String requesterUserInfoMstrId;

	private String status;

	private String techstack;
	
	private String description;
	
	private String projectUrl;
	
	private String appAccessUrl;
	
	private List<UserDetailsBean> userDetails;
	
	private ErrorMessage errorDetails;

	public ProjectBean(ProjectDetails projectDetail, List<UserDetailsBean> userDetails) {
		String dt =  projectDetail.getCreatedTime();
		
		this.projectDetailId = projectDetail.getProjectDetailId();
		this.requesterEmailId = projectDetail.getRequesterEmailId();
		this.createdBy = projectDetail.getCreatedBy();
		this.createdTime = UniqueDateFormatter.format(dt.substring(0,dt.indexOf('T')));
		this.interfaceId = projectDetail.getInterfaceOptions();
		this.projectName = projectDetail.getProjectName();
		this.status = projectDetail.getStatus();
		this.techstack = projectDetail.getTechstack();
		this.description = projectDetail.getDescription();
		this.projectUrl = projectDetail.getProjectUrl();
		this.appAccessUrl = projectDetail.getAppAccessUrl();
		this.userDetails = userDetails;
		
	}



	public ProjectBean(Long projectDetailId, String requesterEmailId, String createdBy, String createdTime, List<InterfaceOption> interfaceId, String projectName,
			 String status, String techstack, List<UserDetailsBean> userDetails) {
		super();
		this.projectDetailId = projectDetailId;
		this.requesterEmailId = requesterEmailId;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.interfaceId = interfaceId;
		this.projectName = projectName;
		this.status = status;
		this.techstack = techstack;
		this.userDetails = userDetails;
	}



	public ProjectBean(Long projectRequestId, String requesterEmailId, String createdBy, String createdTime, List<InterfaceOption> interfaceId, String projectName, String requesterUserInfoMstrId,
			String status, String techstack) {
		super();
		this.projectDetailId = projectRequestId;
		this.requesterEmailId = requesterEmailId;
		this.createdBy = createdBy;
		this.createdTime = createdTime;
		this.interfaceId = interfaceId;
		this.projectName = projectName;
		this.requesterUserInfoMstrId = requesterUserInfoMstrId;
		this.status = status;
		this.techstack = techstack;
	}

	
	
	public ProjectBean() {
		super();
	}


	public Long getProjectRequestId() {
		return projectDetailId;
	}

	public void setProjectRequestId(Long projectRequestId) {
		this.projectDetailId = projectRequestId;
	}

	public String getRequesterEmailId() {
		return requesterEmailId;
	}

	public void setRequesterEmailId(String requesterEmailId) {
		this.requesterEmailId = requesterEmailId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public List<InterfaceOption> getInterfaceId() {
		return interfaceId;
	}

	public void setInterfaceId(List<InterfaceOption> interfaceId) {
		this.interfaceId = interfaceId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getRequesterUserInfoMstrId() {
		return requesterUserInfoMstrId;
	}

	public void setRequesterUserInfoMstrId(String requesterUserInfoMstrId) {
		this.requesterUserInfoMstrId = requesterUserInfoMstrId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTechstack() {
		return techstack;
	}

	public void setTechstack(String techstack) {
		this.techstack = techstack;
	}



	public Long getProjectDetailId() {
		return projectDetailId;
	}



	public void setProjectDetailId(Long projectDetailId) {
		this.projectDetailId = projectDetailId;
	}



	public List<UserDetailsBean> getUserDetails() {
		return userDetails;
	}



	public void setUserDetails(List<UserDetailsBean> userDetails) {
		this.userDetails = userDetails;
	}
	
	
	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getProjectUrl() {
		return projectUrl;
	}



	public void setProjectUrl(String projectUrl) {
		this.projectUrl = projectUrl;
	}



	public String getAppAccessUrl() {
		return appAccessUrl;
	}



	public void setAppAccessUrl(String appAccessUrl) {
		this.appAccessUrl = appAccessUrl;
	}



	public ErrorMessage getErrorDetails() {
		return errorDetails;
	}



	public void setErrorDetails(ErrorMessage errorDetails) {
		this.errorDetails = errorDetails;
	}



	@Override
	public String toString() {
		return "ProjectBean [projectDetailId=" + projectDetailId + ", requesterEmailId=" + requesterEmailId
				+ ", createdBy=" + createdBy + ", createdTime=" + createdTime + ", interfaceId=" + interfaceId
				+ ", projectName=" + projectName + ", requesterUserInfoMstrId=" + requesterUserInfoMstrId + ", status="
				+ status + ", techstack=" + techstack + ", description=" + description + ", projectUrl=" + projectUrl
				+ ", appAccessUrl=" + appAccessUrl + ", userDetails=" + userDetails + "]";
	}


	@Override
	public boolean equals(Object obj){
		try{
			ProjectBean projectBean = (ProjectBean) obj;
			return projectDetailId == projectBean.getProjectDetailId();
		}catch(Exception e){
			return false;
		}
	}


}
