/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 * created on 23-Oct-2018
 */
public class CreateUserRequestBean {

	private String email;
	private String username;
	private String name;
	private boolean reset_password;
	private String password;
	
	public CreateUserRequestBean() {
		super();
	}
	public CreateUserRequestBean(String email, String username, String name, Boolean reset_password, String password) {
		super();
		this.email = email;
		this.username = username;
		this.name = name;
		this.reset_password = reset_password;
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isReset_password() {
		return reset_password;
	}
	public void setReset_password(boolean reset_password) {
		this.reset_password = reset_password;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "CreateUserRequestBean [email=" + email + ", username=" + username + ", name=" + name
				+ ", reset_password=" + reset_password + ", password=" + password + "]";
	}
	
	
}
