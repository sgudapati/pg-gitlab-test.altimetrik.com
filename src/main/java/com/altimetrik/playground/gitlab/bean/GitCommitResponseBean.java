/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 *
 */
public class GitCommitResponseBean {

	private String id;
	private String short_id;
	private String title;
	private String author_name;
	private String author_email;
	private String committer_name;
	private String committer_email;
	private String message;
	private String created_at;
	private String committed_date;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the short_id
	 */
	public String getShort_id() {
		return short_id;
	}
	/**
	 * @param short_id the short_id to set
	 */
	public void setShort_id(String short_id) {
		this.short_id = short_id;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the author_name
	 */
	public String getAuthor_name() {
		return author_name;
	}
	/**
	 * @param author_name the author_name to set
	 */
	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}
	/**
	 * @return the author_email
	 */
	public String getAuthor_email() {
		return author_email;
	}
	/**
	 * @param author_email the author_email to set
	 */
	public void setAuthor_email(String author_email) {
		this.author_email = author_email;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the committer_name
	 */
	public String getCommitter_name() {
		return committer_name;
	}
	/**
	 * @param committer_name the committer_name to set
	 */
	public void setCommitter_name(String committer_name) {
		this.committer_name = committer_name;
	}
	/**
	 * @return the committer_email
	 */
	public String getCommitter_email() {
		return committer_email;
	}
	/**
	 * @param committer_email the committer_email to set
	 */
	public void setCommitter_email(String committer_email) {
		this.committer_email = committer_email;
	}
	/**
	 * @return the created_at
	 */
	public String getCreated_at() {
		return created_at;
	}
	/**
	 * @param created_at the created_at to set
	 */
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	/**
	 * @return the committed_date
	 */
	public String getCommitted_date() {
		return committed_date;
	}
	/**
	 * @param committed_date the committed_date to set
	 */
	public void setCommitted_date(String committed_date) {
		this.committed_date = committed_date;
	}
	
	@Override
	public String toString() {
		return "GitCommitResponseBean [id=" + id + ", short_id=" + short_id + ", title=" + title + ", author_name="
				+ author_name + ", author_email=" + author_email + ", committer_name=" + committer_name
				+ ", committer_email=" + committer_email + ", message=" + message + ", created_at=" + created_at
				+ ", committed_date=" + committed_date + "]";
	}
	
	
}
