/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 * created on 23-Oct-2018
 */
public class CretaeRepoRequestBean {

	private String user_id;
	private String name;
	private String import_url;
	private String visibility;
	
	public CretaeRepoRequestBean(String user_id, String name, String import_url, String visibility) {
		super();
		this.user_id = user_id;
		this.name = name;
		this.import_url = import_url;
		this.visibility = visibility;
	}
	
	public CretaeRepoRequestBean() {
		super();
	}

	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImport_url() {
		return import_url;
	}
	public void setImport_url(String import_url) {
		this.import_url = import_url;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CretaeRepoRequestBean [user_id=" + user_id + ", name=" + name + ", import_url=" + import_url + ", visibility=" + visibility + "]";
	}
	
	
}
