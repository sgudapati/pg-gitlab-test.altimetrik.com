package com.altimetrik.playground.gitlab.bean;

public class Message {

	private String command;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return "Message [command=" + command + "]";
	}
	
}
