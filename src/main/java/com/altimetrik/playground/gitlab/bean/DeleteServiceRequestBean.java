package com.altimetrik.playground.gitlab.bean;

public class DeleteServiceRequestBean {

	private String command;
	
	public DeleteServiceRequestBean(String command) {
		super();
		this.command = command;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return "DeleteServiceRequestBean [command=" + command + "]";
	}
	
}
