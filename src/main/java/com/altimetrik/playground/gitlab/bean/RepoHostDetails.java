package com.altimetrik.playground.gitlab.bean;

public class RepoHostDetails {

	private Status status;
	private Message message;
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "RepoHostDetails [status=" + status + ", message=" + message + "]";
	}
	
}
