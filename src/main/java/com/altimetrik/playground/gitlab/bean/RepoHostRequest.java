/**
 * Copyright (C) Altimetrik 2018. All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Altimetrik. You shall not disclose such Confidential Information
 * and shall use it only in accordance with the terms and conditions
 * entered into with Altimetrik.
 */

package com.altimetrik.playground.gitlab.bean;


public class RepoHostRequest{

	private static final long serialVersionUID = -3563724766110837225L;

	private String command;

	public RepoHostRequest() {
		super();
	}

	public RepoHostRequest(String command) {
		super();
		this.command = command;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public String toString() {
		return "ShellRequest [command=" + command + "]";
	}

}
