/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 * created on 30-Oct-2018
 */
public class GitlabEditFileBean {

	private String branch;
	private String content;
	private String commit_message;
	//private String file_path;
	
	public GitlabEditFileBean() {
		super();
	}
	public GitlabEditFileBean(String branch, String content, String commit_message) {
		super();
		this.branch = branch;
		this.content = content;
		this.commit_message = commit_message;
		//this.file_path=filePath;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCommit_message() {
		return commit_message;
	}
	public void setCommit_message(String commit_message) {
		this.commit_message = commit_message;
	}
	/*public String getFile_path() {
		return file_path;
	}
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	 (non-Javadoc)
	 * @see java.lang.Object#toString()
	 
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GitlabEditFileBean [branch=").append(branch).append(", content=").append(content).append(", commit_message=").append(commit_message).append(", file_path=")
				.append(file_path).append("]");
		return builder.toString();
	}
	*/
	
}
