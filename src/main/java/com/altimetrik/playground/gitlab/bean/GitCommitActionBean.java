/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 *
 */
public class GitCommitActionBean {

	private String action;
	private String file_path;
	private String content;
	
	
	
	/**
	 * @param action
	 * @param file_path
	 * @param content
	 */
	public GitCommitActionBean(String action, String file_path, String content) {
		super();
		this.action = action;
		this.file_path = file_path;
		this.content = content;
	}
	
	public GitCommitActionBean(){
		
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the file_path
	 */
	public String getFile_path() {
		return file_path;
	}
	/**
	 * @param file_path the file_path to set
	 */
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "GitCommitActionBean [action=" + action + ", file_path=" + file_path + ", content=" + content + "]";
	}
	
	
}
