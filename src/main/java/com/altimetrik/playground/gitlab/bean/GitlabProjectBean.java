package com.altimetrik.playground.gitlab.bean;

public class GitlabProjectBean {

	private Long id;
	private String name;
	private String http_url_to_repo;
	private String web_url;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHttp_url_to_repo() {
		return http_url_to_repo;
	}
	public void setHttp_url_to_repo(String http_url_to_repo) {
		this.http_url_to_repo = http_url_to_repo;
	}
	public String getWeb_url() {
		return web_url;
	}
	public void setWeb_url(String web_url) {
		this.web_url = web_url;
	}
	@Override
	public String toString() {
		return "GitlabProjectBean [id=" + id + ", name=" + name + ", http_url_to_repo=" + http_url_to_repo
				+ ", web_url=" + web_url + "]";
	}
	
	/*@Override
	public boolean equals(Object obj){
		if(obj instanceof GitlabProjectBean){
			GitlabProjectBean bean = (GitlabProjectBean) obj;
			if (bean.id == this.id){
				return true;
			}
		}
		return false;
	}*/
}
