/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 * created on 10-Oct-2018
 */
public class ErrorMessage {
	 
	  private String errorCode;
	  private String errorMessage;
	public ErrorMessage(String errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	public ErrorMessage() {
		super();
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	  
}