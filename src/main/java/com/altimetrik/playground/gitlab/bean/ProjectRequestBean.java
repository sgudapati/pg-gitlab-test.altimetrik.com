/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

import java.util.List;
import java.util.Set;

/**
 * @author nmuthusamy
 * created on 12-Oct-2018
 */
public class ProjectRequestBean {

	
	private String requesterEmailId;
	
	private String requesterUserName;
	
	private String requesterUserInfoMstrId;
	
	private String projectName;

	private String techstack;

	private List<Long> interfaceIds;

	private String projectDescription;

	private GitlabCredentials gitlabCredential;
	
	private Set<String> addMemeberList;

	public String getRequesterEmailId() {
		return requesterEmailId;
	}

	public void setRequesterEmailId(String requesterEmailId) {
		this.requesterEmailId = requesterEmailId;
	}

	public String getRequesterUserName() {
		return requesterUserName;
	}

	public void setRequesterUserName(String requesterUserName) {
		this.requesterUserName = requesterUserName;
	}

	public String getRequesterUserInfoMstrId() {
		return requesterUserInfoMstrId;
	}

	public void setRequesterUserInfoMstrId(String requesterUserInfoMstrId) {
		this.requesterUserInfoMstrId = requesterUserInfoMstrId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getTechstack() {
		return techstack;
	}

	public void setTechstack(String techstack) {
		this.techstack = techstack;
	}

	public List<Long> getInterfaceIds() {
		return interfaceIds;
	}

	public void setInterfaceIds(List<Long> interfaceIds) {
		this.interfaceIds = interfaceIds;
	}

	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public GitlabCredentials getGitlabCredential() {
		return gitlabCredential;
	}

	public void setGitlabCredential(GitlabCredentials gitlabCredential) {
		this.gitlabCredential = gitlabCredential;
	}
	
	

	public Set<String> getAddMemeberList() {
		return addMemeberList;
	}

	public void setAddMemeberList(Set<String> addMemeberList) {
		this.addMemeberList = addMemeberList;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProjectRequest [requesterEmailId=" + requesterEmailId + ", requesterUserName=" + requesterUserName + ", requesterUserInfoMstrId=" + requesterUserInfoMstrId
				+ ", projectName=" + projectName + ", techstack=" + techstack + ", interfaceIds=" + interfaceIds + ", projectDescription=" + projectDescription
				+ ", gitlabCredential=" + gitlabCredential + ", addMemeberList=" + addMemeberList + "]";
	}

	
	
}

