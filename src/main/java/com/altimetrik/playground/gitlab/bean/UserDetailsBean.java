/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

import com.altimetrik.playground.gitlab.entity.UserDetail;

/**
 * @author nmuthusamy
 * created on 24-Oct-2018
 */
public class UserDetailsBean {

	private Long userDetailId;
	private String gitEmailId;
	private String name;
	private String gitUsername;
	private String gitPassword;
	private String email;
	
	public UserDetailsBean(UserDetail user) {
		
		this.userDetailId = user.getUserDetailId();
		this.gitEmailId = user.getGitEmailId();
		this.name = user.getName();
		this.gitUsername = user.getGitUsername();
		this.gitPassword = user.getGitPassword();
		this.email = user.getEmail();
	}

	
	
	public UserDetailsBean(Long userDetailId, String gitEmailId, String name, String gitUsername, String gitPassword, String email) {
		super();
		this.userDetailId = userDetailId;
		this.gitEmailId = gitEmailId;
		this.name = name;
		this.gitUsername = gitUsername;
		this.gitPassword = gitPassword;
		this.email = email;
	}



	public UserDetailsBean() {
		super();
	}

	public Long getUserDetailId() {
		return userDetailId;
	}

	public void setUserDetailId(Long userDetailId) {
		this.userDetailId = userDetailId;
	}

	public String getGitEmailId() {
		return gitEmailId;
	}

	public void setGitEmailId(String gitEmailId) {
		this.gitEmailId = gitEmailId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGitUsername() {
		return gitUsername;
	}

	public void setGitUsername(String gitUsername) {
		this.gitUsername = gitUsername;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getGitPassword() {
		return gitPassword;
	}

	public void setGitPassword(String gitPassword) {
		this.gitPassword = gitPassword;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UserDetailsBean [userDetailId=" + userDetailId + ", gitEmailId=" + gitEmailId + ", name=" + name + ", gitUsername=" + gitUsername + ", email=" + email + "]";
	}
	
	
}
