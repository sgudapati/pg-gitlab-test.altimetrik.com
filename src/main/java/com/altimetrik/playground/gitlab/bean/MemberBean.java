package com.altimetrik.playground.gitlab.bean;

public class MemberBean {

	private String memberEmailId;
	private String memberRole;

	public String getMemberEmailId() {
		return memberEmailId;
	}

	public void setMemberEmailId(String memberEmailId) {
		this.memberEmailId = memberEmailId;
	}

	public String getMemberRole() {
		return memberRole;
	}

	public void setMemberRole(String memberRole) {
		this.memberRole = memberRole;
	}

	@Override
	public String toString() {
		return "MemberBean [memberEmailId=" + memberEmailId + ", memberRole=" + memberRole + "]";
	}
	
	
}
