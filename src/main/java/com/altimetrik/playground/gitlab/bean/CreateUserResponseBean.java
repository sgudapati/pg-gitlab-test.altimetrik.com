/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 * created on 23-Oct-2018
 */
public class CreateUserResponseBean {

	private String id;
	private String email;
	private String password;
	private Boolean reset_password ;
	private String username;
	private String name;
	private int projects_limit;
	private String state;
	private String created_at;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getReset_password() {
		return reset_password;
	}
	public void setReset_password(Boolean reset_password) {
		this.reset_password = reset_password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getProjects_limit() {
		return projects_limit;
	}
	public void setProjects_limit(int projects_limit) {
		this.projects_limit = projects_limit;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreateUserResponseBean [id=" + id + ", email=" + email + ", password=" + password + ", reset_password=" + reset_password + ", username=" + username + ", name="
				+ name + ", projects_limit=" + projects_limit + ", state=" + state + ", created_at=" + created_at + "]";
	}
	
	
}
