/**
 * 
 */
package com.altimetrik.playground.gitlab.bean;

/**
 * @author nmuthusamy
 * created on 15-Oct-2018
 */
public class GitlabCredentials {

	private String gitlabEmailId;
	private String gitlabUserName;
	
	public String getGitLabEmailId() {
		return gitlabEmailId;
	}
	public void setGitLabEmailId(String gitLabEmailId) {
		this.gitlabEmailId = gitLabEmailId;
	}
	public String getGitLabUserName() {
		return gitlabUserName;
	}
	public void setGitLabUserName(String gitLabUserName) {
		this.gitlabUserName = gitLabUserName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GitLabCredentialBean [gitLabEmailId=" + gitlabEmailId + ", gitLabUserName=" + gitlabUserName + "]";
	}
	
	
}
