/**
 * 
 */
package com.altimetrik.playground.gitlab.exception;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.altimetrik.playground.gitlab.bean.ErrorMessage;

/**
 * @author nmuthusamy created on 10-Oct-2018
 */
@RestControllerAdvice
public class AltiExpectionHandler extends Exception {

	private static final long serialVersionUID = 2571988898939103032L;
	private static final Logger LOG = LoggerFactory.getLogger(AltiExpectionHandler.class);

	@ExceptionHandler(value = NotFoundException.class)
	public final ResponseEntity<ErrorMessage> handleNotFoundException(NotFoundException ex) {
		LOG.error("ERROR-NotFoundException");
		LOG.info(ex.getLocalizedMessage());
		LOG.info(ex.getMessage());
		ErrorMessage errorDetails = new ErrorMessage(ex.getMessage(), "No data found for the given request.");
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(AltiException.class)
	public final ResponseEntity<ErrorMessage> handleAltiExceptions(Exception ex) {
		LOG.error("ERROR-Alti Exception");
		LOG.info(ex.getLocalizedMessage());
		LOG.info(ex.getMessage());
		ErrorMessage errorDetails = new ErrorMessage(ex.getMessage(), ex.getLocalizedMessage());
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorMessage> handleAllExceptions(Exception ex) {
		LOG.error("ERROR-Exception");
		LOG.info(ex.getLocalizedMessage());
		LOG.info(ex.getMessage());
		ex.printStackTrace();
		ErrorMessage errorDetails = new ErrorMessage(ex.getMessage(), ex.getLocalizedMessage());
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
