package com.altimetrik.playground.gitlab.exception;

public class AltiException extends Exception {

	private static final long serialVersionUID = 1L;

	public AltiException(String exception) {
		super(exception);
	}

}
