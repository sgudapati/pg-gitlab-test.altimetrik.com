/**
 * 
 */
package com.altimetrik.playground.gitlab.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.altimetrik.playground.gitlab.entity.InterfaceOption;

/**
 * @author nmuthusamy
 * created on 16-Oct-2018
 */
@Repository
public interface InterfaceOptionsRepository extends JpaRepository<InterfaceOption, Long>{

	@Query("SELECT i FROM InterfaceOption i WHERE i.interfaceId IN :ids")
	List<InterfaceOption> findByIdsIn(@Param("ids") List<Long> ids);
	
	InterfaceOption findOneByInterfaceType(String interfaceOption);
}
