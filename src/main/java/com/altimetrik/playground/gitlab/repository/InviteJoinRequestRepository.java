package com.altimetrik.playground.gitlab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.altimetrik.playground.gitlab.entity.InviteJoinRequest;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;

@Repository
public interface InviteJoinRequestRepository extends JpaRepository<InviteJoinRequest, Long>{

	@Query(value = "SELECT * FROM invite_join_request WHERE status = :status AND execution_count < 2 ORDER BY 'created_on' LIMIT 1 ", nativeQuery = true)
	public InviteJoinRequest findOneByStatus(@Param("status") String status); 
}
