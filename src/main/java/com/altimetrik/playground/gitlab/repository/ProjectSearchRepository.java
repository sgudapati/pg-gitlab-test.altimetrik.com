package com.altimetrik.playground.gitlab.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.service.impl.ProjectServiceImpl;

@Repository
public class ProjectSearchRepository {

	private static final Logger LOG = LoggerFactory.getLogger(ProjectSearchRepository.class);
	
	@PersistenceContext
    private EntityManager em;
	
	public List<ProjectDetails> searchData(List<String> owners, List<String> techstacks, List<String> projectName){

		StringBuffer bf = new StringBuffer("SELECT p FROM ProjectDetails p where ");
		boolean firstOwner = true;
		if(owners!=null){
			bf.append("(");
			for (String owner : owners) {
				if(firstOwner){
					bf.append("p.requesterEmailId like '%"+owner+"%'");
					firstOwner = false;
				}else{
					bf.append("OR p.requesterEmailId like '%"+owner+"%'");
				}
			}
			bf.append(")");
		}
		
		boolean firstTecstack = true;
		if(techstacks!=null){
			if(owners != null) 
				bf.append("AND");
			bf.append("(");
			for (String techstack : techstacks) {
				if(firstTecstack){
					bf.append("p.techstack like '%"+techstack+"%'");
					firstTecstack = false;
				}else{
					bf.append("OR p.techstack like '%"+techstack+"%'");
				}
			}
			bf.append(")");
		}
		
		boolean firstprojectName = true;
		if(projectName!=null){
			if(owners != null || techstacks != null) 
				bf.append("AND");
			bf.append("(");
			for (String project : projectName) {
				if(firstprojectName){
					bf.append("p.projectName like '%"+project+"%'");
					firstprojectName = false;
				}else{
					bf.append("OR p.projectName like '%"+project+"%'");
				}
			}
			bf.append(")");
		}
		LOG.info("Query string :: " + bf.toString());
		TypedQuery<ProjectDetails> query = em.createQuery(bf.toString(), ProjectDetails.class);
			  List<ProjectDetails> results = query.getResultList();
		LOG.info("Total data retreived : " + results.size());
		return results;
		
	}
}
