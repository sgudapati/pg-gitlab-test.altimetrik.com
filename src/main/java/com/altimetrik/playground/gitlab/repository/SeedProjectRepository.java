/**
 * 
 */
package com.altimetrik.playground.gitlab.repository;


import com.altimetrik.playground.gitlab.entity.SeedProject;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * @author nmuthusamy
 * created on 10-Oct-2018
 */
@Repository
public interface SeedProjectRepository extends JpaRepository<SeedProject,Long>{

	@Query("SELECT s.technology FROM SeedProject s")
	List<String> getTechStack();
	
	List<SeedProject> findByTechnology(String technology);
}
