/**
 * 
 */
package com.altimetrik.playground.gitlab.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.altimetrik.playground.gitlab.entity.ProjectDetails;

/**
 * @author nmuthusamy
 * created on 16-Oct-2018
 */
@Repository
public interface ProjectRepository extends JpaRepository<ProjectDetails, Long>{

	@Query(value = "SELECT * FROM project_details WHERE status = :status AND execution_count < 2 ORDER BY 'created_time' LIMIT 1 ", nativeQuery = true)
	public ProjectDetails findOneByStatus(@Param("status") String status); 
	
	@Query(value = "SELECT * FROM project_details WHERE status = :status AND execution_count < 1 ORDER BY 'created_time' LIMIT 20 ", nativeQuery = true)
	public List<ProjectDetails> findTwentyByStatus(@Param("status") String status); 
	
	@Query(value = "SELECT p FROM ProjectDetails p WHERE p.status = :status ORDER BY p.createdTime DESC")
	public List<ProjectDetails> findAllByStatus(@Param("status") String status);
	
	public List<ProjectDetails> findAllByRequesterEmailId(String emailId);
	
	public ProjectDetails findOneByProjectDetailId(Long id);
	
	@Query("SELECT p FROM ProjectDetails p WHERE p.projectDetailId IN :ids ORDER BY p.createdTime DESC")
	public List<ProjectDetails> findAllByProjectDetailId(@Param("ids")Set<Long> ids);
	
	public long countByProjectName(final String projectName);
	
	
	@Query(value = "SELECT * FROM project_details WHERE status = :status AND project_name like %:projectName% LIMIT 50", nativeQuery = true)
	public List<ProjectDetails> findCreatedProjects(@Param("status") String status, @Param("projectName") String projectName);
	
}
