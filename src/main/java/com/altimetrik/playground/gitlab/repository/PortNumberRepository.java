/**
 * 
 */
package com.altimetrik.playground.gitlab.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.altimetrik.playground.gitlab.entity.PortNumbers;

/**
 * @author nmuthusamy
 * created on 30-Oct-2018
 */
@Repository
public interface PortNumberRepository extends CrudRepository<PortNumbers, Long>{

	@Query(value = "SELECT * FROM port_numbers ORDER BY created_date DESC LIMIT 1", nativeQuery = true)
	PortNumbers findLastUsedPort();
}
