/**
 * 
 */
package com.altimetrik.playground.gitlab.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.altimetrik.playground.gitlab.entity.UserDetail;

/**
 * @author nmuthusamy
 * created on 22-Oct-2018
 */
@Repository
public interface UserdetailsRepository extends JpaRepository<UserDetail, Long>{

	public UserDetail findByGitEmailId(String emailId);
	
	public UserDetail findOneByUserDetailId(Long userDetailsId);
	
	public List<UserDetail> findByGitEmailIdIn(List<String> gitEmailId);
}
