/**
 * 
 */
package com.altimetrik.playground.gitlab.email;

/**
 * @author nmuthusamy
 *
 */
public class EmailCustomization {
	private Object emailContent;
	

	public EmailCustomization(Object emailContent) {
		super();
		this.emailContent = emailContent;
	}

	public EmailCustomization() {
		super();
	}

	/**
	 * @return the emailContent
	 */
	public Object getEmailContent() {
		return emailContent;
	}

	/**
	 * @param emailContent the emailContent to set
	 */
	public void setEmailContent(Object emailContent) {
		this.emailContent = emailContent;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailCustomization [emailContent=" + emailContent + "]";
	}
	
}
