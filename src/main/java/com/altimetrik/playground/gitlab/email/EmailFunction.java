package com.altimetrik.playground.gitlab.email;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.altimetrik.playground.gitlab.entity.InviteJoinRequest;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.entity.UserDetail;
import com.altimetrik.playground.gitlab.enums.EnumList;

/**
 * @author nmuthusamy
 *
 */
@Component
public class EmailFunction {

	private static final Logger LOG = LoggerFactory.getLogger(EmailFunction.class);
	@Value("${playground.serviceUrl}")
	private String playgroundUrl;

	static RestTemplate restTemplate = new RestTemplate();

	public void createRepoEmailNotify(ProjectDetails projectDetails) {
		String url = playgroundUrl + "/messenger/sendEmail";
		LOG.info("Sending email notification for project creation : " + url);
		if (projectDetails != null) {

			String userName = projectDetails.getRequesterEmailId().substring(0,	projectDetails.getRequesterEmailId().indexOf('@'));
		
			ProjectCreationEmailContent emailContent = new ProjectCreationEmailContent(Long.toString(projectDetails.getGitProjectId()), projectDetails.getProjectName(),
					projectDetails.getUserDetail().get(0).getGitUsername(), projectDetails.getProjectUrl(), userName);
			
			EmailCustomization emailCustomization = new EmailCustomization(emailContent);
			
			EmailNotificationBean emailNotification = new EmailNotificationBean(EnumList.REPO_CREATE_EMAIL_NOTIFICATION.getValue(), "Altimetrik Playground",
					projectDetails.getRequesterEmailId(), playgroundUrl, emailCustomization);
			
			LOG.info("Email notification request body :" + emailNotification.toString());
			
			this.startEmailThread(emailNotification, projectDetails.getRequesterEmailId());
		}
	}

	public void addMemberToProjectEmailNotify(ProjectDetails projectDetails, InviteJoinRequest request, UserDetail userDetail) {
		String url = playgroundUrl + "/messenger/sendEmail";
		LOG.info("Sending email notification for adding member to project : " + url);
		if (projectDetails != null) {

			String userName = request.getMemberEmailId().substring(0,	request.getMemberEmailId().indexOf('@'));
		
			JoinMemberEmailContent emailContent = new JoinMemberEmailContent(Long.toString(projectDetails.getGitProjectId()), projectDetails.getProjectName(),
					userDetail.getGitUsername(), projectDetails.getProjectUrl(), userName, "DEVELOPER");
			
			EmailCustomization emailCustomization = new EmailCustomization(emailContent);
			
			EmailNotificationBean emailNotification = new EmailNotificationBean(EnumList.MEMBER_ADDED_EMAIL_NOTIFICATION.getValue(), "Altimetrik Playground",
					request.getMemberEmailId(), playgroundUrl, emailCustomization);
			
			LOG.info("Email notification request body :" + emailNotification.toString());
			
			this.startEmailThread(emailNotification, projectDetails.getRequesterEmailId());
		}
	}
	
	public void startEmailThread(EmailNotificationBean emailNotification, String requesterEmailId) {
		
		String url = playgroundUrl + "/messenger/sendEmail";

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<?> entity = new HttpEntity<>(emailNotification, headers);

		ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
		/**
		 * Email is being sent in a separate thread. Next lines of code will
		 * continue to execute without waiting for the response from email
		 * server
		 */
		emailExecutor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					LOG.info("Rest URL to send email : " + url);
					ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity,
							String.class);

					if (responseEntity.getStatusCodeValue() == 200)
						LOG.info("Email sent successfully");
					else
						LOG.info("Email not sent");

				} catch (Exception e) {
					LOG.error("Error Sending email notification to " + requesterEmailId);
					LOG.info("EXCEPTION : " + e.getLocalizedMessage());
					LOG.info("EXCEPTION : " + e.getMessage());
					e.printStackTrace();
				}
			}
		});
		emailExecutor.shutdown();

	}
}
