/**
 * 
 */
package com.altimetrik.playground.gitlab.email;

/**
 * @author nmuthusamy
 *
 */
public class JoinMemberEmailContent {

	private String applyGitProjectId;
	private String applyGitProjectName;
	private String applyGitUserName;
	private String applyGitProjectUrl;
	private String applyUserName;
	private String applyGitMemberRole;
	
	public JoinMemberEmailContent() {
		super();
	}
	public JoinMemberEmailContent(String applyGitProjectId, String applyGitProjectName, String applyGitUserName, String applyGitProjectUrl, String applyUserName, String applyGitMemberRole) {
		super();
		this.applyGitProjectId = applyGitProjectId;
		this.applyGitProjectName = applyGitProjectName;
		this.applyGitUserName = applyGitUserName;
		this.applyGitProjectUrl = applyGitProjectUrl;
		this.applyUserName = applyUserName;
		this.applyGitMemberRole = applyGitMemberRole;
	}

	
	
	public String getApplyUserName() {
		return applyUserName;
	}
	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}
	public String getApplyGitProjectId() {
		return applyGitProjectId;
	}
	public void setApplyGitProjectId(String applyGitProjectId) {
		this.applyGitProjectId = applyGitProjectId;
	}
	public String getApplyGitProjectName() {
		return applyGitProjectName;
	}
	public void setApplyGitProjectName(String applyGitProjectName) {
		this.applyGitProjectName = applyGitProjectName;
	}
	public String getApplyGitUserName() {
		return applyGitUserName;
	}
	public void setApplyGitUserName(String applyGitUserName) {
		this.applyGitUserName = applyGitUserName;
	}
	public String getApplyGitProjectUrl() {
		return applyGitProjectUrl;
	}
	public void setApplyGitProjectUrl(String applyGitProjectUrl) {
		this.applyGitProjectUrl = applyGitProjectUrl;
	}
	
	public String getApplyGitMemberRole() {
		return applyGitMemberRole;
	}
	public void setApplyGitMemberRole(String applyGitMemberRole) {
		this.applyGitMemberRole = applyGitMemberRole;
	}
	@Override
	public String toString() {
		return "JoinMemberEmailContent [applyGitProjectId=" + applyGitProjectId + ", applyGitProjectName="
				+ applyGitProjectName + ", applyGitUserName=" + applyGitUserName + ", applyGitProjectUrl="
				+ applyGitProjectUrl + ", applyUserName=" + applyUserName + ", applyGitMemberRole=" + applyGitMemberRole
				+ "]";
	}
	
	
	
	
}