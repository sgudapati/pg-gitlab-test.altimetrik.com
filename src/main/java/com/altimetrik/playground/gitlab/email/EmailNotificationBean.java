
package com.altimetrik.playground.gitlab.email;

/**
 * @author nmuthusamy
 *
 */
public class EmailNotificationBean {

	private String caseId;
	private String sourceApp;
	private String toId;
	private String pgurl;
	private EmailCustomization emailCustomization;
	/**
	 * @param caseId
	 * @param sourceApp
	 * @param toId
	 * @param pgurl
	 * @param emailCustomization
	 */
	public EmailNotificationBean(String caseId, String sourceApp, String toId, String pgurl,
			EmailCustomization emailCustomization) {
		super();
		this.caseId = caseId;
		this.sourceApp = sourceApp;
		this.toId = toId;
		this.pgurl = pgurl;
		this.emailCustomization = emailCustomization;
	}
	/**
	 * 
	 */
	public EmailNotificationBean() {
		super();
	}
	/**
	 * @return the caseId
	 */
	public String getCaseId() {
		return caseId;
	}
	/**
	 * @param caseId the caseId to set
	 */
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	/**
	 * @return the sourceApp
	 */
	public String getSourceApp() {
		return sourceApp;
	}
	/**
	 * @param sourceApp the sourceApp to set
	 */
	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}
	/**
	 * @return the toId
	 */
	public String getToId() {
		return toId;
	}
	/**
	 * @param toId the toId to set
	 */
	public void setToId(String toId) {
		this.toId = toId;
	}
	/**
	 * @return the pgurl
	 */
	public String getPgurl() {
		return pgurl;
	}
	/**
	 * @param pgurl the pgurl to set
	 */
	public void setPgurl(String pgurl) {
		this.pgurl = pgurl;
	}
	/**
	 * @return the emailCustomization
	 */
	public EmailCustomization getEmailCustomization() {
		return emailCustomization;
	}
	/**
	 * @param emailCustomization the emailCustomization to set
	 */
	public void setEmailCustomization(EmailCustomization emailCustomization) {
		this.emailCustomization = emailCustomization;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailNotificationBean [caseId=" + caseId + ", sourceApp=" + sourceApp + ", toId=" + toId + ", pgurl="
				+ pgurl + ", emailCustomization=" + emailCustomization + "]";
	}
	
	
}
