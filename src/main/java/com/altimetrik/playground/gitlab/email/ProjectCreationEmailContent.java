/**
 * 
 */
package com.altimetrik.playground.gitlab.email;

/**
 * @author nmuthusamy
 *
 */
public class ProjectCreationEmailContent {

	private String applyGitProjectId;
	private String applyGitProjectName;
	private String applyGitUserName;
	private String applyGitProjectUrl;
	private String applyUserName;
	
	public ProjectCreationEmailContent() {
		super();
	}
	public ProjectCreationEmailContent(String applyGitProjectId, String applyGitProjectName, String applyGitUserName, String applyGitProjectUrl, String applyUserName) {
		super();
		this.applyGitProjectId = applyGitProjectId;
		this.applyGitProjectName = applyGitProjectName;
		this.applyGitUserName = applyGitUserName;
		this.applyGitProjectUrl = applyGitProjectUrl;
		this.applyUserName = applyUserName;
	}

	
	
	public String getApplyUserName() {
		return applyUserName;
	}
	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}
	public String getApplyGitProjectId() {
		return applyGitProjectId;
	}
	public void setApplyGitProjectId(String applyGitProjectId) {
		this.applyGitProjectId = applyGitProjectId;
	}
	public String getApplyGitProjectName() {
		return applyGitProjectName;
	}
	public void setApplyGitProjectName(String applyGitProjectName) {
		this.applyGitProjectName = applyGitProjectName;
	}
	public String getApplyGitUserName() {
		return applyGitUserName;
	}
	public void setApplyGitUserName(String applyGitUserName) {
		this.applyGitUserName = applyGitUserName;
	}
	public String getApplyGitProjectUrl() {
		return applyGitProjectUrl;
	}
	public void setApplyGitProjectUrl(String applyGitProjectUrl) {
		this.applyGitProjectUrl = applyGitProjectUrl;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmailContent [applyGitProjectId=");
		builder.append(applyGitProjectId);
		builder.append(", applyGitProjectName=");
		builder.append(applyGitProjectName);
		builder.append(", applyGitUserName=");
		builder.append(applyGitUserName);
		builder.append(", applyGitProjectUrl=");
		builder.append(applyGitProjectUrl);
		builder.append(", userName=");
		builder.append(applyUserName);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}