package com.altimetrik.playground.gitlab.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the user_details database table.
 * 
 */
@Entity
@Table(name="user_details")
@NamedQuery(name="UserDetail.findAll", query="SELECT u FROM UserDetail u")
public class UserDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_detail_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long userDetailId;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private String createdDate;

	@Column(name="git_email_id")
	private String gitEmailId;

	@Column(name="git_user_id")
	private String gitUserId;

	@Column(name="name")
	private String name;

	@Column(name="updated_by")
	private String updatedBy;

	@Column(name="updated_date")
	private String updatedDate;

	@Column(name="user_info_mstr_id")
	private String userInfoMstrId;

	@Column(name="git_username")
	private String gitUsername;
	
	@Column(name="git_password")
	private String gitPassword;
	
	@Column(name="status")
	private String status;
	

	/*@ManyToMany(cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name="project_user_mapping", joinColumns = { @JoinColumn(name="user_detail_id")}, inverseJoinColumns={@JoinColumn(name="project_detail_id")})
	@JsonBackReference
	private List<ProjectDetails> projectDetails = new ArrayList<>();*/
	
	
	@ManyToMany(fetch=FetchType.LAZY)
	@JoinTable(name="project_user_mapping", joinColumns = { @JoinColumn(name="user_detail_id")}, inverseJoinColumns={@JoinColumn(name="project_detail_id")})
	@JsonManagedReference
	private List<ProjectDetails> projectDetails = new ArrayList<>();
	
	public UserDetail(String gitEmailId, String gitUsername) {
		super();
		this.gitEmailId = gitEmailId;
		this.gitUsername = gitUsername;
	}
	
	

	public UserDetail(String createdBy, String createdDate, String gitEmailId, String gitUserId, String name, String gitUsername, String gitPassword) {
		super();
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.gitEmailId = gitEmailId;
		this.gitUserId = gitUserId;
		this.name = name;
		this.gitUsername = gitUsername;
		this.gitPassword = gitPassword;
	}

	public UserDetail(String createdBy, String createdDate, String gitEmailId, String gitUserId, String name, String gitUsername) {
		super();
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.gitEmailId = gitEmailId;
		this.gitUserId = gitUserId;
		this.name = name;
		this.gitUsername = gitUsername;
	}


	public String getGitEmailId() {
		return gitEmailId;
	}



	public void setGitEmailId(String gitEmailId) {
		this.gitEmailId = gitEmailId;
	}



	public String getGitUsername() {
		return gitUsername;
	}



	public void setGitUsername(String gitUsername) {
		this.gitUsername = gitUsername;
	}



	public UserDetail() {
	}

	public Long getUserDetailId() {
		return this.userDetailId;
	}

	public void setUserDetailId(Long userDetailId) {
		this.userDetailId = userDetailId;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getEmail() {
		return this.gitEmailId;
	}

	public void setEmail(String email) {
		this.gitEmailId = email;
	}

	public String getGitUserId() {
		return this.gitUserId;
	}

	public void setGitUserId(String gitUserId) {
		this.gitUserId = gitUserId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUserInfoMstrId() {
		return this.userInfoMstrId;
	}

	public void setUserInfoMstrId(String userInfoMstrId) {
		this.userInfoMstrId = userInfoMstrId;
	}

	public String getUsername() {
		return this.gitUsername;
	}

	public void setUsername(String username) {
		this.gitUsername = username;
	}


	public List<ProjectDetails> getProjectDetails() {
		return projectDetails;
	}



	public void setProjectDetails(List<ProjectDetails> projectDetails) {
		this.projectDetails = projectDetails;
	}
	
	

	public String getGitPassword() {
		return gitPassword;
	}



	public void setGitPassword(String gitPassword) {
		this.gitPassword = gitPassword;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	@Override
	public boolean equals(Object obj){
		try{
			UserDetail userDetail = (UserDetail) obj;
			return userDetailId == userDetail.getUserDetailId();
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserDetail [userDetailId=").append(userDetailId).append(", createdBy=").append(createdBy).append(", createdDate=").append(createdDate)
				.append(", gitEmailId=").append(gitEmailId).append(", gitUserId=").append(gitUserId).append(", name=").append(name).append(", updatedBy=").append(updatedBy)
				.append(", updatedDate=").append(updatedDate).append(", userInfoMstrId=").append(userInfoMstrId).append(", gitUsername=").append(gitUsername).append("]");
		return builder.toString();
	}

}