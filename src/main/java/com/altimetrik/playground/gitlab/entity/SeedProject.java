package com.altimetrik.playground.gitlab.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the seed_projects database table.
 * 
 */
@Entity
@Table(name="seed_projects")
@NamedQuery(name="SeedProject.findAll", query="SELECT s FROM SeedProject s")
public class SeedProject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="seed_project_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "gitlab_project_id")
	private Long gitlabProjectId;
	
	
	@Column(name="clone_url")
	@JsonIgnore
	private String cloneUrl;

	@Column(name="project_name")
	private String projectName;

	@Column(name="technology")
	private String technology;
	
	@Column(name = "description")
	private String description;
	
	@OneToOne
	@JoinColumn(name="default_interface")
	private InterfaceOption defaultInterface;
	
	@ManyToMany
	@JoinTable(name="seed_project_interface_mapping", joinColumns = { @JoinColumn(name="seed_project_id")}, inverseJoinColumns={@JoinColumn(name="interface_id")})
	private List<InterfaceOption> interfaceOptions=new ArrayList<>();

	public SeedProject() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCloneUrl() {
		return this.cloneUrl;
	}

	public void setCloneUrl(String cloneUrl) {
		this.cloneUrl = cloneUrl;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getTechnology() {
		return this.technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public List<InterfaceOption> getInterfaceOptions() {
		return interfaceOptions;
	}

	public void setInterfaceOptions(List<InterfaceOption> interfaceOptions) {
		this.interfaceOptions = interfaceOptions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public InterfaceOption getDefaultInterface() {
		return defaultInterface;
	}

	public void setDefaultInterface(InterfaceOption defaultInterface) {
		this.defaultInterface = defaultInterface;
	}

	public Long getGitlabProjectId() {
		return gitlabProjectId;
	}

	public void setGitlabProjectId(Long gitlabProjectId) {
		this.gitlabProjectId = gitlabProjectId;
	}

	
}