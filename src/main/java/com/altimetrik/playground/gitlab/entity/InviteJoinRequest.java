package com.altimetrik.playground.gitlab.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the invite_join_request database table.
 * 
 */
@Entity
@Table(name="invite_join_request")
@NamedQuery(name="InviteJoinRequest.findAll", query="SELECT i FROM InviteJoinRequest i")
public class InviteJoinRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="invite_join_request_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long inviteJoinRequestId;

	@Column(name="project_detail_id")
	private Long projectDetailId;

	@Column(name="added_by")
	private String addedBy;

	@Column(name="status")
	private String status;
	
	@Column(name="member_email_id")
	private String memberEmailId;
	
	@Column(name="member_role")
	private String memberRole;

	@Column(name="created_on")
	private String createdOn;
	
	@Column(name="updated_on")
	private String updatedOn;
	
	@Column(name="execution_count")
	private int executionCount;
	
	
	public InviteJoinRequest(Long projectDetailId, String requestedBy, String status, String userEmailId,String memberRole, String createdOn) {
		super();
		this.projectDetailId = projectDetailId;
		this.addedBy = requestedBy;
		this.status = status;
		this.memberEmailId = userEmailId;
		this.memberRole = memberRole;
		this.createdOn = createdOn;
	}

	public InviteJoinRequest() {
	}

	public Long getInviteJoinRequestId() {
		return this.inviteJoinRequestId;
	}

	public void setInviteJoinRequestId(Long inviteJoinRequestId) {
		this.inviteJoinRequestId = inviteJoinRequestId;
	}

	public Long getProjectDetailId() {
		return this.projectDetailId;
	}

	public void setProjectDetailId(Long projectDetailId) {
		this.projectDetailId = projectDetailId;
	}

	public String getRequestedBy() {
		return this.addedBy;
	}

	public void setRequestedBy(String requestedBy) {
		this.addedBy = requestedBy;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserEmailId() {
		return this.memberEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.memberEmailId = userEmailId;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

	public int getExecutionCount() {
		return executionCount;
	}

	public void setExecutionCount(int executionCount) {
		this.executionCount = executionCount;
	}

	public String getMemberEmailId() {
		return memberEmailId;
	}

	public void setMemberEmailId(String memberEmailId) {
		this.memberEmailId = memberEmailId;
	}

	public String getMemberRole() {
		return memberRole;
	}

	public void setMemberRole(String memberRole) {
		this.memberRole = memberRole;
	}

	
}