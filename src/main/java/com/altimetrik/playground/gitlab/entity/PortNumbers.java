package com.altimetrik.playground.gitlab.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the port_number_list database table.
 * 
 */
@Entity
@Table(name="port_numbers")
@NamedQuery(name="PortNumbers.findAll", query="SELECT p FROM PortNumbers p")
public class PortNumbers implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="port_number_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idPortNumberList;

	@Column(name="created_date")
	private String createdDate;

	@Column(name="port_number")
	private String portNumber;

	@Column(name="project_id")
	private Long projectId;

	public PortNumbers() {
	}
	
	public PortNumbers(String createdDate, String portNumber, Long projectId) {
		super();
		this.createdDate = createdDate;
		this.portNumber = portNumber;
		this.projectId = projectId;
	}

	public Long getIdPortNumberList() {
		return this.idPortNumberList;
	}

	public void setIdPortNumberList(Long idPortNumberList) {
		this.idPortNumberList = idPortNumberList;
	}

	public String getEnvironment() {
		return this.createdDate;
	}

	public void setEnvironment(String environment) {
		this.createdDate = environment;
	}

	public String getPortNumber() {
		return this.portNumber;
	}

	public void setPortNumber(String portNumber) {
		this.portNumber = portNumber;
	}

	public Long getProjectId() {
		return this.projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

}