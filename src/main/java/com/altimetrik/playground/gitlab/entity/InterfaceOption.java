package com.altimetrik.playground.gitlab.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the interface_options database table.
 * 
 */
@Entity
@Table(name="interface_options")
@NamedQuery(name="InterfaceOption.findAll", query="SELECT i FROM InterfaceOption i")
public class InterfaceOption implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="interface_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long interfaceId;

	
	@Column(name="interface_type")
	private String interfaceType;

	public InterfaceOption() {
	}

	public Long getInterfaceId() {
		return this.interfaceId;
	}

	public void setInterfaceId(Long interfaceId) {
		this.interfaceId = interfaceId;
	}

	public String getInterfaceType() {
		return this.interfaceType;
	}

	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "InterfaceOption [interfaceId=" + interfaceId + ", interfaceType=" + interfaceType + "]";
	}

}