package com.altimetrik.playground.gitlab.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the project_request database table.
 * 
 */
@Entity
@Table(name="project_details")
@NamedQuery(name="ProjectDetails.findAll", query="SELECT p FROM ProjectDetails p")
public class ProjectDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="project_detail_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long projectDetailId;
	
	@Column(name="requester_email_id")
	private String requesterEmailId;
	
	@Column(name="requester_user_info_mstr_id")
	private String requesterUserInfoMstrId;
	
	@Column(name="techstack")
	private String techstack;
	
	@Column(name="project_name")
	private String projectName;
	
	@Column(name="status")
	private String status;
	
	@Column(name="execution_count")
	private Integer executionCount;
	
	@Column(name="description")
	private String description;
	
	@Column(name="git_project_id")
	private Long gitProjectId;
	
	@Column(name="project_url")
	private String projectUrl;
	
	
	@Column(name="created_time")
	private String createdTime;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="updated_by")
	private String updatedBy;

	@Column(name="updated_time")
	private String updatedTime;
	
	@Column(name="app_access_url")
	private String appAccessUrl;
	
	@Column(name="members_count")
	private int membersCount;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name="project_interface_mapping", joinColumns = { @JoinColumn(name="project_detail_id")}, inverseJoinColumns={@JoinColumn(name="interface_id")})
	private List<InterfaceOption> interfaceOptions = new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name="project_user_mapping", joinColumns = { @JoinColumn(name="project_detail_id")}, inverseJoinColumns={@JoinColumn(name="user_detail_id")})
	@JsonBackReference
	private List<UserDetail> userDetail = new ArrayList<>();
	
	public ProjectDetails() {
		super();
	}

	
	public ProjectDetails(String requesterEmailId, String requesterUserInfoMstrId, String techstack, String projectName, String status, String description, String createdTime,
			String createdBy, int executionCount) {
		super();
		this.requesterEmailId = requesterEmailId;
		this.requesterUserInfoMstrId = requesterUserInfoMstrId;
		this.techstack = techstack;
		this.projectName = projectName;
		this.status = status;
		this.description = description;
		this.createdTime = createdTime;
		this.createdBy = createdBy;
		this.executionCount = executionCount;
	}


	public Long getProjectDetailId() {
		return projectDetailId;
	}


	public void setProjectDetailId(Long projectDetailId) {
		this.projectDetailId = projectDetailId;
	}


	public String getRequesterEmailId() {
		return requesterEmailId;
	}


	public void setRequesterEmailId(String requesterEmailId) {
		this.requesterEmailId = requesterEmailId;
	}


	public String getRequesterUserInfoMstrId() {
		return requesterUserInfoMstrId;
	}


	public void setRequesterUserInfoMstrId(String requesterUserInfoMstrId) {
		this.requesterUserInfoMstrId = requesterUserInfoMstrId;
	}


	public String getTechstack() {
		return techstack;
	}


	public void setTechstack(String techstack) {
		this.techstack = techstack;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Long getGitProjectId() {
		return gitProjectId;
	}


	public void setGitProjectId(Long gitProjectId) {
		this.gitProjectId = gitProjectId;
	}


	public String getCreatedTime() {
		return createdTime;
	}


	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


	public String getUpdatedTime() {
		return updatedTime;
	}


	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}


	public List<InterfaceOption> getInterfaceOptions() {
		return interfaceOptions;
	}


	public void setInterfaceOptions(List<InterfaceOption> interfaceOptions) {
		this.interfaceOptions = interfaceOptions;
	}


	public List<UserDetail> getUserDetail() {
		return userDetail;
	}


	public void setUserDetail(List<UserDetail> userDetail) {
		this.userDetail = userDetail;
	}


	public String getProjectUrl() {
		return projectUrl;
	}


	public void setProjectUrl(String projectUrl) {
		this.projectUrl = projectUrl;
	}

	public Integer getExecutionCount() {
		return executionCount;
	}


	public void setExecutionCount(Integer executionCount) {
		this.executionCount = executionCount;
	}


	public String getAppAccessUrl() {
		return appAccessUrl;
	}


	public void setAppAccessUrl(String appAccessUrl) {
		this.appAccessUrl = appAccessUrl;
	}


	public int getMembersCount() {
		return membersCount;
	}


	public void setMembersCount(int membersCount) {
		this.membersCount = membersCount;
	}


	@Override
	public String toString() {
		return "ProjectDetails [projectDetailId=" + projectDetailId + ", requesterEmailId=" + requesterEmailId
				+ ", requesterUserInfoMstrId=" + requesterUserInfoMstrId + ", techstack=" + techstack + ", projectName="
				+ projectName + ", status=" + status + ", executionCount=" + executionCount + ", description="
				+ description + ", gitProjectId=" + gitProjectId + ", projectUrl=" + projectUrl + ", createdTime="
				+ createdTime + ", createdBy=" + createdBy + ", updatedBy=" + updatedBy + ", updatedTime=" + updatedTime
				+ ", appAccessUrl=" + appAccessUrl + ", interfaceOptions=" + interfaceOptions + ", userDetail="
				+ userDetail + "]";
	}


	


}