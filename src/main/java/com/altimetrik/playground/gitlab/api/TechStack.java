/**
 * 
 */
package com.altimetrik.playground.gitlab.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.playground.gitlab.entity.SeedProject;
import com.altimetrik.playground.gitlab.service.TechStackService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author nmuthusamy
 * created on 09-Oct-2018
 */
@RestController
@RequestMapping(value = "/techStacks")
@Api(tags={"Techstacks"}, value = "/techstack", description = "All about techstack availability")
public class TechStack {

	@Autowired
	TechStackService techStackService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = { "application/json" })
	@ApiOperation(value = "Get all techstack",
	notes = "Get list of all techstack available in the system",
	response = SeedProject.class,
    responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Not Found"),
		    @ApiResponse(code = 200, message = "Success") 
	})
	public ResponseEntity<List<SeedProject>> getAllSeedProject() {
		List<SeedProject> list = techStackService.getAllTechnology();
		return new ResponseEntity<List<SeedProject>>(list, HttpStatus.OK);
	}
	
}
