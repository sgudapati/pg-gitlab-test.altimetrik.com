/**
 * 
 */
package com.altimetrik.playground.gitlab.api;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.playground.gitlab.bean.AddMemberRequestBean;
import com.altimetrik.playground.gitlab.bean.ErrorMessage;
import com.altimetrik.playground.gitlab.bean.ProjectBean;
import com.altimetrik.playground.gitlab.bean.ProjectRequestBean;
import com.altimetrik.playground.gitlab.entity.InviteJoinRequest;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
//import com.altimetrik.playground.gitlab.entity.ProjectUserMapping;
import com.altimetrik.playground.gitlab.enums.EnumList;
import com.altimetrik.playground.gitlab.exception.AltiException;
import com.altimetrik.playground.gitlab.service.InviteJoinService;
import com.altimetrik.playground.gitlab.service.ProjectService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author nmuthusamy
 * created on 12-Oct-2018
 */
@RestController
@RequestMapping(value="/projects")
@Api(tags={"Projects"}, description = "All about Project creation and modification")
public class Project {
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	InviteJoinService inviteJoinService;

	@RequestMapping(value = "/", method = RequestMethod.POST, produces = { "application/json" }, consumes = { "application/json"})
	@ApiOperation(value = "Submit new project request",
	notes = "Requesting for a new project/repo creation",
	response = ProjectBean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success"), 
			@ApiResponse(code = 409, message = "Duplicate entry"),
			@ApiResponse(code = 500, message = "Internal Serve	r Error")
	})
	public ResponseEntity<ProjectBean> submitProjectRequest(@ApiParam(value = "Project request object", required = true) @RequestBody ProjectRequestBean request) {
		ProjectBean response;
		HttpStatus status = HttpStatus.OK;
		try {
			response = projectService.submitRequest(request);
		} catch (AltiException ex) {
			response = new ProjectBean();
			response.setErrorDetails(new ErrorMessage(ex.getMessage(), ex.getMessage()));
			status = HttpStatus.FORBIDDEN;
		}
		return new ResponseEntity<ProjectBean>(response, status);
	}
	
	@RequestMapping(value = "/all/{emailId}", method = RequestMethod.GET, produces = { "application/json" })
	@ApiOperation(value = "Get list of available projects",
	notes = "Get list of all available projects in which given emailId is not associated with.",
	response = ProjectBean.class,
    responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Not Found"),
		    @ApiResponse(code = 200, message = "Success") 
	})
	public ResponseEntity<List<ProjectBean>> getAllProject(@PathVariable("emailId") String emailId) {
		return new ResponseEntity<List<ProjectBean>>(projectService.getAllProject(emailId),HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{emailId}", method = RequestMethod.GET, produces = { "application/json" })
	@ApiOperation(value = "Get list of available projects for given emailId",
	notes = "Get list of all available projects for given emailId in the system.",
	response = ProjectBean.class,
    responseContainer = "List")
	@ApiResponses(value = {
			@ApiResponse(code = 500, message = "Internal Server Error"),
			@ApiResponse(code = 404, message = "Not Found"),
		    @ApiResponse(code = 200, message = "Success")
	})
	public ResponseEntity<List<ProjectBean>> getAllProjectByEmail(@ApiParam(value = "emailId for which projects need to be retreived", required = true) @PathVariable("emailId")String emailId) {
		List<ProjectBean> res = projectService.getOneUseProjects(emailId);
		return new ResponseEntity<List<ProjectBean>>(res,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createProject", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<ProjectBean>> getByStatus() throws AltiException {
		projectService.createProject();
		return null;
	}
	
	@RequestMapping(value="/{projectId}/invite",method=RequestMethod.POST, produces = { "application/json" },consumes = { "application/json" })
	@ApiOperation(value = "Invite members to project",
	notes = "Invite single or multiple user to the project by Owner of the project",
	response = InviteJoinRequest.class,
	responseContainer = "List")
	public ResponseEntity<List<InviteJoinRequest>> addMember(@RequestBody AddMemberRequestBean request) throws AltiException{
		List<InviteJoinRequest> res = inviteJoinService.inviteMember(request,EnumList.OWNER.getValue());
		return new ResponseEntity<List<InviteJoinRequest>>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{projectId}/join",method=RequestMethod.POST, produces = { "application/json" },consumes = { "application/json" })
	@ApiOperation(value = "Invite members to project",
	notes = "Invite single or multiple user to the project by Owner of the project",
	response = InviteJoinRequest.class,
	responseContainer = "List")
	public ResponseEntity<List<InviteJoinRequest>> joinToProject(@RequestBody AddMemberRequestBean request) throws AltiException{
		List<InviteJoinRequest> res = inviteJoinService.inviteMember(request,EnumList.SELF.getValue());
		return new ResponseEntity<List<InviteJoinRequest>>(res, HttpStatus.OK);
	}

	@RequestMapping(value="/{projectId}/users/{userId}",method=RequestMethod.DELETE, produces = { "application/json" },consumes = { "application/json" })
	@ApiOperation(value = "Remove members from project",
	notes = "Remove single user from project by Owner of the project",
	response = ProjectDetails.class)
	public ResponseEntity<ProjectDetails> removeMemberFromProject(@PathVariable("projectId") Long projectId, @PathVariable("userId") Long userId){
		ProjectDetails response = inviteJoinService.removeUserFromProject(projectId, userId);
		return new ResponseEntity<ProjectDetails>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value="/projectDetails/{projectId}",method=RequestMethod.GET, produces = { "application/json" })
	@ApiOperation(value = "Get single project details",
	notes = "Get single project and single user",
	response = ProjectBean.class)
	public ResponseEntity<List<ProjectBean>> getOneProject(@PathVariable("projectId") Long projectId){
		List<ProjectBean> response = projectService.getOneProject(projectId);
		return new ResponseEntity<List<ProjectBean>>(response,HttpStatus.OK);
	}
	
	@RequestMapping(value="/{projectId}/{userId}",method=RequestMethod.GET, produces = { "application/json" })
	@ApiOperation(value = "Get single project and Single user",
	notes = "Get single project and single user",
	response = ProjectBean.class)
	public ResponseEntity<List<ProjectBean>> getProjectAndMember(@PathVariable("projectId") Long projectId, @PathVariable("userId") Long userId){
		List<ProjectBean> response = projectService.getProjectAndUser(projectId, userId);
		return new ResponseEntity<List<ProjectBean>>(response,HttpStatus.OK);
	}

	@RequestMapping(value="/search",method=RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<List<ProjectBean>> searcProjects(@RequestParam(value = "owner", required = false) List<String> owner,
			@RequestParam(value = "techstack", required = false) List<String> techstack,
			@RequestParam(value = "name", required = false) List<String> projectName){
		List<ProjectBean> res = projectService.searchProject(owner, techstack, projectName);
		return new ResponseEntity<List<ProjectBean>>(res,HttpStatus.OK);
	}
	
}

