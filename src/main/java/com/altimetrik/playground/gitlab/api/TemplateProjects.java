package com.altimetrik.playground.gitlab.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.playground.gitlab.service.TemplateProjectsService;

@RestController
@RequestMapping(value = "/templateProjects")
public class TemplateProjects {

	private static final Logger LOG = LoggerFactory.getLogger(TemplateProjects.class);
	
	@Autowired
	TemplateProjectsService templateProjectService;

	@RequestMapping(value = "/update", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> updateSeedProject() {
		try {
			templateProjectService.updateTemplateProjectList();
			return new ResponseEntity<>("One Template updated in DB...", HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("ERROR updating template project");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			return new ResponseEntity<>("Error updating template projects...", HttpStatus.BAD_REQUEST);
		}
	}
}
