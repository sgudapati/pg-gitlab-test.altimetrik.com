package com.altimetrik.playground.gitlab.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.altimetrik.playground.gitlab.bean.UserDetailsBean;
import com.altimetrik.playground.gitlab.service.UserService;

@RestController
@RequestMapping(value = "/users")
public class Users {

	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/{gitlabEmailId}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<UserDetailsBean> getUserDetailByGitlabEmailId(@PathVariable("gitlabEmailId") String gitlabEmailId){
		
		return new ResponseEntity<>(userService.getUserByGitEmailId(gitlabEmailId), HttpStatus.OK);
		
	}
}
