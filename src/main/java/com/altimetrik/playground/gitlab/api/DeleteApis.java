/**
 * 
 */
package com.altimetrik.playground.gitlab.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.altimetrik.playground.gitlab.bean.GitlabUserBean;
import com.altimetrik.playground.gitlab.bean.ProjectRequestBean;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.exception.AltiException;
import com.altimetrik.playground.gitlab.repository.ProjectRepository;
import com.altimetrik.playground.gitlab.service.DeleteService;
import com.altimetrik.playground.gitlab.service.ProjectService;
import com.altimetrik.playground.gitlab.util.GitFunctions;
import com.altimetrik.playground.gitlab.util.UrlBuilder;

/**
 * @author nmuthusamy
 *
 */
@RestController
@RequestMapping(value = "/delete")
public class DeleteApis {

	@Autowired
	ProjectRepository projectRepo;

	@Autowired
	UrlBuilder urlBuilder;

	@Autowired
	ProjectService projectService;
	
	@Autowired
	DeleteService deleteService;

	@RequestMapping(value = "/{projectNameLike}", method = RequestMethod.DELETE, produces = { "application/json" })
	public ResponseEntity<String> getAllProject(@PathVariable("projectNameLike") String projectNameLike) {

		List<ProjectDetails> projectList = projectRepo.findCreatedProjects("REPO_CREATED", projectNameLike);
		for (ProjectDetails project : projectList) {
			String res = GitFunctions.deleteProject(
					urlBuilder.getDeleteProjectUrl(Long.toString(project.getGitProjectId())), urlBuilder.getHeader());
			if (res.equals("SUCCESS")) {
				project.setStatus("DELETED");
				projectRepo.save(project);
			}

		}
		return new ResponseEntity<>("Success...", HttpStatus.OK);

	}

	@RequestMapping(value = "/usersAndTheirProjects", method = RequestMethod.DELETE, produces = { "application/json" })
	public ResponseEntity<List<String>> deleteProjects(@RequestParam("file") MultipartFile readExcelDataFile)
			throws IOException {
		try {
			List<String> projects = deleteService.deleteUsersAndProjects(readExcelDataFile);
			return new ResponseEntity<List<String>>(projects, HttpStatus.OK);
		}catch (Exception e) {
		
			
			return new ResponseEntity<List<String>>(HttpStatus.BAD_REQUEST);
		
		}
		
		
		

	}
}
