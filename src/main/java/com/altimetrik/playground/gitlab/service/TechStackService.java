/**
 * 
 */
package com.altimetrik.playground.gitlab.service;

import java.util.List;

import com.altimetrik.playground.gitlab.entity.SeedProject;

/**
 * @author nmuthusamy
 * created on 10-Oct-2018
 */

public interface TechStackService {

	public List<SeedProject> getAllTechnology();
}
