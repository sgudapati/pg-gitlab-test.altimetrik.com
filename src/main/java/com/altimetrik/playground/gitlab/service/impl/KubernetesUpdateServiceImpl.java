package com.altimetrik.playground.gitlab.service.impl;

import java.io.File;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.altimetrik.playground.gitlab.bean.CreateRepoResponseBean;
import com.altimetrik.playground.gitlab.bean.GitCommitActionBean;
import com.altimetrik.playground.gitlab.bean.GitCommitRequestBean;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.enums.EnumList;
import com.altimetrik.playground.gitlab.enums.KbsFilepathEnum;
import com.altimetrik.playground.gitlab.repository.ProjectRepository;
import com.altimetrik.playground.gitlab.service.KubernetesUpdateService;
import com.altimetrik.playground.gitlab.util.GitFunctions;
import com.altimetrik.playground.gitlab.util.UrlBuilder;

@Service
public class KubernetesUpdateServiceImpl implements KubernetesUpdateService{

	private static final Logger LOG = LoggerFactory.getLogger(KubernetesUpdateServiceImpl.class);
	
	@Autowired
	ProjectRepository projectRepo;
	
	@Autowired
	UrlBuilder urlBuilder;
	
	@Value("${gitlab.javaGitlabCiFilePath}")
	private String javaGitlabCiFilePath;
	
	@Value("${gitlab.jsGitlabCiFilePath}")
	private String jsGitlabCiFilePath;
	
	@Value("${gitlab.javaValuesFilePath}")
	private String javaValuesFilePath;
	
	@Value("${gitlab.jsValuesFilePath}")
	private String jsValuesFilePath;
	
	@Value("${gitlab.deploymentUrl}")
	private String deploymentUrl;
	
	@Override
	//@Async
	public void editAndUpdateKbsConfigFiles(CreateRepoResponseBean repo, ProjectDetails projectDetails) {
		LOG.info("Inside editAndUpdateServiceName method...");
		GitCommitRequestBean request = new GitCommitRequestBean();
		GitCommitActionBean[] action;
		if(projectDetails.getTechstack().equals("Angular7") || projectDetails.getTechstack().equals("Angular5"))
			action = new GitCommitActionBean[11];
		else if(projectDetails.getTechstack().equals("ReactJS16"))
			action = new GitCommitActionBean[12];
		else
			action = new GitCommitActionBean[9];
		
		String gitProjectId = Long.toString(projectDetails.getGitProjectId());
		String serviceName = EnumList.SERVICE_NAME.getValue()+gitProjectId;
		LOG.info("Unique service name : " + serviceName);
		
		String helpersContent = this.getHelpersContent(serviceName);
		GitCommitActionBean actionZero = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.HELPERS_TPL_FILE_PATH.getValue(), helpersContent);
		action[0] = actionZero;
		
		String chartContent = this.getChartContent(serviceName);
		GitCommitActionBean actionOne = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.CHART_YML_FILE_PATH.getValue(), chartContent);
		action[1] = actionOne;
		
		String testConnectionContent = this.getTestConnectionContent(serviceName);
		GitCommitActionBean actionTwo = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.TEST_CONNECTION_FILE_PATH.getValue(), testConnectionContent);
		action[2] = actionTwo;
		
		String deploymentContent = this.getDeploymentContent(serviceName);
		GitCommitActionBean actionThree = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.DEPLOYMENT_FILE_PATH.getValue(), deploymentContent);
		action[3] = actionThree;
		
		String ingressContent = this.getIngressContent(serviceName);
		GitCommitActionBean actionFour = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.INGRESS_FILE_PATH.getValue(), ingressContent);
		action[4] = actionFour;
		
		String notesContent = this.getNotesContent(serviceName);
		GitCommitActionBean actionFive = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.NOTES_FILE_PATH.getValue(), notesContent);
		action[5] = actionFive;
		
		String serviceContent = this.getServiceContent(serviceName);
		GitCommitActionBean actionSix = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.SERVICE_FILE_PATH.getValue(), serviceContent);
		action[6] = actionSix;
		
		String valueContent = this.getValuesContent(serviceName, projectDetails.getTechstack());
		GitCommitActionBean actionSeven = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.VALUES_FILE_PATH.getValue(), valueContent);
		action[7] = actionSeven;
		
		String gitlabCiContent = this.getGitlabCiContent(serviceName, projectDetails.getTechstack());
		GitCommitActionBean actionEight = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.GITLAB_CI_FILE_PATH.getValue(), gitlabCiContent);
		action[8] = actionEight;
		
		if(projectDetails.getTechstack().equals("Angular7") || 
				projectDetails.getTechstack().equals("Angular5") ||
				projectDetails.getTechstack().equals("ReactJS16")){
			String defaultContent = this.getDefaultContent(serviceName);
			GitCommitActionBean actionNine = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.DEFAULT_FILE_PATH.getValue(), defaultContent);
			action[9] = actionNine;
			
			String dockerfileContent = this.getDockerfileContent(serviceName, projectDetails.getTechstack());
			GitCommitActionBean actionTen = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.DOCKERFILE_FILE_PATH.getValue(), dockerfileContent);
			action[10] = actionTen;
		}
		
		if(projectDetails.getTechstack().equals("ReactJS16")){
			String packageContent = this.getPackageContent(serviceName);
			GitCommitActionBean actionEleven = new GitCommitActionBean(EnumList.UPDATE.getValue(), KbsFilepathEnum.PACKAGE_FILE_PATH.getValue(), packageContent);
			action[11] = actionEleven;
		}
		
		request.setBranch("master");
		request.setCommit_message("service names updated");
		request.setId(Long.toString(repo.getId()));
		request.setActions(action);
		GitFunctions.gitlabCommitFile(request, urlBuilder.gitCommitUrl(Long.toString(repo.getId())), urlBuilder.getHeader());
		
		ProjectDetails project = projectRepo.findOneByProjectDetailId(projectDetails.getProjectDetailId());
		LOG.info("Project details from DB : " + project.toString());
		if(projectDetails.getTechstack().equals("Java8_SpringBoot2.0.5")){
			project.setAppAccessUrl(deploymentUrl+ serviceName +"/v1/hello/");
		}else if(projectDetails.getTechstack().equals("Angular7") || 
				projectDetails.getTechstack().equals("Angular5")||
				projectDetails.getTechstack().equals("ReactJS16")){
			project.setAppAccessUrl(deploymentUrl+ serviceName);
		}
		LOG.info("Data to be saved to DB : " + project.toString());
		projectRepo.save(project);
		
		
	}

	@SuppressWarnings("resource")
	private String getGitlabCiContent(String serviceName, String techStack) {
		LOG.info("getGitlabCiContent - START");
		String gitlabCiContent = null;
		String content = null;
		try {
			switch (techStack) {
			case "Java8_SpringBoot2.0.5":
				gitlabCiContent = new Scanner(new File(javaGitlabCiFilePath)).useDelimiter("\\Z").next();
				break;
			case "Angular7":
				gitlabCiContent = new Scanner(new File(jsGitlabCiFilePath)).useDelimiter("\\Z").next();
				break;
			case "Angular5":
				gitlabCiContent = new Scanner(new File(jsGitlabCiFilePath)).useDelimiter("\\Z").next();
				break;
			case "ReactJS16":
				gitlabCiContent = new Scanner(new File(jsGitlabCiFilePath)).useDelimiter("\\Z").next();
				break;
			default:
				break;
			}
			
			content = gitlabCiContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading gitlabCi content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getGitlabCiContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getTestConnectionContent(String serviceName) {
		LOG.info("getTestConnectionContent - START");
		String testConnectionContent = null;
		String content = null;
		try {
			testConnectionContent = new Scanner(new File("commitFiles/test-connection.txt")).useDelimiter("\\Z").next();
			content = testConnectionContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading testConnectionContent content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getTestConnectionContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getValuesContent(String serviceName, String techStack) {
		LOG.info("getValuesContent - START");
		String valuesContent = null;
		String content = null;
		try {
			switch (techStack) {
			case "Java8_SpringBoot2.0.5":
				valuesContent = new Scanner(new File(javaValuesFilePath)).useDelimiter("\\Z").next();
				break;
			case "Angular7":
				valuesContent = new Scanner(new File(jsValuesFilePath)).useDelimiter("\\Z").next();
				break;
			case "Angular5":
				valuesContent = new Scanner(new File(jsValuesFilePath)).useDelimiter("\\Z").next();
				break;
			case "ReactJS16":
				valuesContent = new Scanner(new File(jsValuesFilePath)).useDelimiter("\\Z").next();
			default:
				break;
			}
			
			content = valuesContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error values content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getValuesContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getDockerfileContent(String serviceName, String techStack) {
		LOG.info("getDockerfileContent - START");
		String dockerfileContent = null;
		String content = null;
		try {
			switch (techStack) {
			case "Angular7":
				dockerfileContent = new Scanner(new File("commitFiles/angular_dockerfile.txt")).useDelimiter("\\Z").next();
				break;
			case "Angular5":
				dockerfileContent = new Scanner(new File("commitFiles/angular_dockerfile.txt")).useDelimiter("\\Z").next();
				break;
			case "ReactJS16":
				dockerfileContent = new Scanner(new File("commitFiles/react_dockerfile.txt")).useDelimiter("\\Z").next();
				break;
			default:
				break;
			}
			
			content = dockerfileContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading dockerfile content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getDockerfileContent - END");
		return content;
	}
	@SuppressWarnings("resource")
	private String getServiceContent(String serviceName) {
		LOG.info("getServiceContent - START");
		String serviceContent = null;
		String content = null;
		try {
			serviceContent = new Scanner(new File("commitFiles/service.txt")).useDelimiter("\\Z").next();
			content = serviceContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading service content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getServiceContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getNotesContent(String serviceName) {
		LOG.info("getNotesContent - START");
		String notesContent = null;
		String content = null;
		try {
			notesContent = new Scanner(new File("commitFiles/NOTES.txt")).useDelimiter("\\Z").next();
			content = notesContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading Notes content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getNotesContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getIngressContent(String serviceName) {
		LOG.info("getIngressContent - START");
		String ingressContent = null;
		String content = null;
		try {
			ingressContent = new Scanner(new File("commitFiles/ingress.txt")).useDelimiter("\\Z").next();
			content = ingressContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading ingress content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getIngressContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getDeploymentContent(String serviceName) {
		LOG.info("getDeploymentContent - START");
		String deploymentContent = null;
		String content = null;
		try {
			deploymentContent = new Scanner(new File("commitFiles/deployment.txt")).useDelimiter("\\Z").next();
			content = deploymentContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading deployment content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getDeploymentContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getChartContent(String serviceName) {
		LOG.info("getChartContent - START");
		String chartContent = null;
		String content = null;
		try {
			chartContent = new Scanner(new File("commitFiles/Chart.txt")).useDelimiter("\\Z").next();
			content = chartContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading chart content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getChartContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getHelpersContent(String serviceName) {
		LOG.info("getHelpersContent - START");
		String helpersContent = null;
		String content = null;
		try {
			helpersContent = new Scanner(new File("commitFiles/helpers.txt")).useDelimiter("\\Z").next();
			content = helpersContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading helper content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getHelpersContent - END");
		return content;
	}
	
	@SuppressWarnings("resource")
	private String getDefaultContent(String serviceName) {
		LOG.info("getDefaultContent - START");
		String defaultContent = null;
		String content = null;
		try {
			defaultContent = new Scanner(new File("commitFiles/angular_default.txt")).useDelimiter("\\Z").next();
			content = defaultContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading default content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getDefaultContent - END");
		return content;
	}

	@SuppressWarnings("resource")
	private String getPackageContent(String serviceName) {
		LOG.info("getPackageContent - START");
		String packageContent = null;
		String content = null;
		try {
			packageContent = new Scanner(new File("commitFiles/react_package.txt")).useDelimiter("\\Z").next();
			content = packageContent.replaceAll("SERVICE_NAME", serviceName);
		} catch (Exception e) {
			LOG.error("Error reading package content");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		LOG.info("getPackageContent - END");
		return content;
	}

}
