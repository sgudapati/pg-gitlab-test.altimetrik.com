package com.altimetrik.playground.gitlab.service;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Service;

import com.altimetrik.playground.gitlab.bean.Request;

@Service
public interface service {

	List<List<String>> getReport(Request request) throws IOException;

}
