package com.altimetrik.playground.gitlab.service;

import com.altimetrik.playground.gitlab.bean.CreateRepoResponseBean;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;

public interface KubernetesUpdateService {
	public void editAndUpdateKbsConfigFiles(CreateRepoResponseBean repo, ProjectDetails projectDetails);
}
