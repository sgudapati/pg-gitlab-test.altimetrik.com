/**
 * 
 */
package com.altimetrik.playground.gitlab.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.altimetrik.playground.gitlab.bean.CreateRepoResponseBean;
import com.altimetrik.playground.gitlab.bean.CreateUserRequestBean;
import com.altimetrik.playground.gitlab.bean.CreateUserResponseBean;
import com.altimetrik.playground.gitlab.bean.CretaeRepoRequestBean;
import com.altimetrik.playground.gitlab.bean.GitlabUserBean;
import com.altimetrik.playground.gitlab.bean.ProjectBean;
import com.altimetrik.playground.gitlab.bean.ProjectRequestBean;
import com.altimetrik.playground.gitlab.bean.UserDetailsBean;
import com.altimetrik.playground.gitlab.email.EmailFunction;
import com.altimetrik.playground.gitlab.entity.InterfaceOption;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.entity.SeedProject;
import com.altimetrik.playground.gitlab.entity.UserDetail;
import com.altimetrik.playground.gitlab.enums.EnumList;
import com.altimetrik.playground.gitlab.enums.StatusEnum;
import com.altimetrik.playground.gitlab.enums.RepoVisibilityLevel;
import com.altimetrik.playground.gitlab.enums.TechstackEnum;
import com.altimetrik.playground.gitlab.exception.AltiException;
import com.altimetrik.playground.gitlab.exception.NotFoundException;
import com.altimetrik.playground.gitlab.repository.ProjectSearchRepository;
import com.altimetrik.playground.gitlab.repository.InterfaceOptionsRepository;
import com.altimetrik.playground.gitlab.repository.ProjectRepository;
import com.altimetrik.playground.gitlab.repository.SeedProjectRepository;
import com.altimetrik.playground.gitlab.repository.UserdetailsRepository;
import com.altimetrik.playground.gitlab.service.KubernetesUpdateService;
import com.altimetrik.playground.gitlab.service.PortNumberService;
import com.altimetrik.playground.gitlab.service.ProjectService;
import com.altimetrik.playground.gitlab.util.GitFunctions;
import com.altimetrik.playground.gitlab.util.KubernitesThread;
import com.altimetrik.playground.gitlab.util.UniqueDateFormatter;
import com.altimetrik.playground.gitlab.util.UrlBuilder;

/**
 * @author nmuthusamy created on 16-Oct-2018
 */
@Service
public class ProjectServiceImpl implements ProjectService {

	private static final Logger LOG = LoggerFactory.getLogger(ProjectServiceImpl.class);
	
	@Autowired
	ProjectSearchRepository searchRepo;
	
	@Autowired
	InterfaceOptionsRepository interfaceOptionRepo;

	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	UserdetailsRepository userDetailsRepo;
	
	@Autowired
	SeedProjectRepository seedRepo;

	@Autowired
	UrlBuilder urlBuilder;
	
	@Autowired
	PortNumberService portNumberService;
	
	@Autowired
	EmailFunction emailFunction;
	
	@Autowired
	KubernetesUpdateService kbsService;
	
	@Autowired
	KubernitesThread kbsThread;
	
	@Override
	@Transactional
	public ProjectBean submitRequest(ProjectRequestBean request) throws AltiException {
		LOG.info("Inside submitRequest method...");
		LOG.info("Request Body : " + request.toString());
		if (this.projectRepository.countByProjectName(request.getProjectName()) > 0) {
			LOG.info("Project with the name already exist : "+request.getProjectName());
			throw new AltiException("project name taken");
		}
		ProjectDetails projectRequest = new ProjectDetails(request.getRequesterEmailId(), request.getRequesterUserInfoMstrId(), request.getTechstack(), request.getProjectName(),
				StatusEnum.NEW.getValue(), request.getProjectDescription(), UniqueDateFormatter.formatDate(new Date()), request.getRequesterUserName(),Integer.parseInt(EnumList.ZERO.getValue()));
		LOG.info("Getting InterfaceOption details for interface ID : " + request.getInterfaceIds());
		List<InterfaceOption> interfaceList = interfaceOptionRepo.findAllById(request.getInterfaceIds());
		LOG.info("Getting InterfaceOption details SUCCESS " + interfaceList.toString());
		projectRequest.setInterfaceOptions(interfaceList);

		if (request.getGitlabCredential() != null) {
			LOG.info("Request bean contains gitlab credentials along with it...");
			List<UserDetail> userDetails = new ArrayList<>();
			LOG.info("Getting the user details based on the gitlab credentials provided...");
			UserDetail user = userDetailsRepo.findByGitEmailId(request.getGitlabCredential().getGitLabEmailId());
			LOG.info("Getting the user details based on the gitlab credentials provided - SUCCESS");
			if (user != null) {
				LOG.info("User exists in Playground_EE DB");
				userDetails.add(user);
			}
			else {
				LOG.info("User doesnt exists in Playground_EE DB. Inserting new entry into the DB");
				userDetails.add(new UserDetail(request.getGitlabCredential().getGitLabEmailId(), request.getGitlabCredential().getGitLabUserName()));
			}
			projectRequest.setUserDetail(userDetails);
		}else {
			this.validateUser(projectRequest, new UserDetail());
		}
		LOG.info("Project Data to be saved into DB : " + projectRequest.toString());
		ProjectDetails projectDetails = projectRepository.save(projectRequest);
		LOG.info("Project Data saved - SUCCESS " + projectDetails.toString());
		ProjectBean response = new ProjectBean(projectDetails.getProjectDetailId(), projectDetails.getRequesterEmailId(), 
				projectDetails.getCreatedBy(), projectDetails.getCreatedTime(), projectDetails.getInterfaceOptions(), 
				projectDetails.getProjectName(), projectDetails.getRequesterUserInfoMstrId(), projectDetails.getStatus(), 
				projectDetails.getTechstack());
		return response;
	}

	//ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
	
	ExecutorService executor = Executors.newFixedThreadPool(3);

	
	@Override
	@Scheduled(cron = "${cron.pattern.createProject}")
	public void createProject() {
		
		String timestamp = UniqueDateFormatter.formatDate(new Date());
		LOG.info("createProject CRON job invoked at TIMESTAMP: " + timestamp);
		LOG.info("Getting project from the DB which is in NEW status");
		List<ProjectDetails> projectDetails = projectRepository.findTwentyByStatus("NEW");
		LOG.info("Getting NEW projeect SUCCESS");
		if (!projectDetails.isEmpty()) {
			projectDetails.forEach(p -> p.setExecutionCount(1));
			projectRepository.saveAll(projectDetails);
			for (ProjectDetails project : projectDetails) {
				Runnable task = () ->{
						this.createProjectFunction(project);
				};
				executor.submit(task);
			}
			//executor.shutdown();
			
		}else{
			LOG.info(timestamp+" There is no project in DB with status NEW.");
		}
	}
	
	private void validateUser(ProjectDetails projectDetails,UserDetail userDetail ) {
		LOG.info("No user associated to the submitted ProjectRequest. Checking for existing user with requester email ID in system DB: " + projectDetails.getRequesterEmailId());
		UserDetail user = userDetailsRepo.findByGitEmailId(projectDetails.getRequesterEmailId());
		if(user == null){
			LOG.info("No Gitlab user existing with requester email ID in system DB.");
			LOG.info("Checking for user with email ID : " + projectDetails.getRequesterEmailId() + " in the gitlab.");
			List<GitlabUserBean> userList = GitFunctions.searchOneUserByEmail(urlBuilder.searchUserByEmailUrl(projectDetails.getRequesterEmailId()), urlBuilder.getHeader());
			if(!userList.isEmpty()){
				LOG.info("User exists in gitlab.");
				GitlabUserBean gitlabUser = userList.get(0);
				userDetail = new UserDetail(EnumList.SYSTEM.getValue(), gitlabUser.getCreated_at(), gitlabUser.getEmail(), 
						Long.toString(gitlabUser.getId()), gitlabUser.getName(), gitlabUser.getUsername(),EnumList.GITLAB_DEFAULT_PASSWORD.getValue());
			}
			else{
				LOG.info("No user exists in gitlab with email ID : "+projectDetails.getRequesterEmailId() + ". Creating new user");
				CreateUserResponseBean userBean = createGitlabUser(projectDetails);
				userDetail = new UserDetail(EnumList.SYSTEM.getValue(), userBean.getCreated_at(), userBean.getEmail(), userBean.getId(), userBean.getName(), 
						userBean.getUsername(), EnumList.GITLAB_DEFAULT_PASSWORD.getValue()); 
				}

			List<UserDetail> userDetails = new ArrayList<>();
			userDetails.add(userDetail);
			projectDetails.setUserDetail(userDetails);
		}else{
			LOG.info("Requester email ID already has an account in Gitlab");
			String email = user.getGitEmailId();
			userDetail = getAndUpdateUserDetails(email);
			List<UserDetail> userDetails = new ArrayList<>();
			userDetails.add(userDetail);
			projectDetails.setUserDetail(userDetails);
		}
	}
	
	//@Async
	@Retryable(value = { Exception.class }, maxAttempts = 2, backoff = @Backoff(delay = 5000))
	public void createProjectFunction(ProjectDetails projectDetails){
		LOG.info("ProjectDetails : "+projectDetails.toString());
		LOG.info("Execution count for the projct request is being increased.");
		//this.updateExecutionCount(projectDetails);
		UserDetail userDetail = new UserDetail();
		if (projectDetails.getUserDetail().isEmpty()) {
			this.validateUser(projectDetails, userDetail);
		}else{
			LOG.info("ProjectDetails has user associated to it.");
			String email = projectDetails.getUserDetail().get(0).getEmail();
			userDetail = getAndUpdateUserDetails(email);
		}
		
		if(userDetail != null){
			int port = Integer.parseInt(portNumberService.getLastUsedPort().getPortNumber())+1;
			CreateRepoResponseBean repo = this.createRepoForUser(projectDetails, userDetail, port);
			if(projectDetails.getTechstack().equals("Java8_SpringBoot2.0.5") ||
					projectDetails.getTechstack().equals("Angular7") ||
					projectDetails.getTechstack().equals("Angular5") ||
					projectDetails.getTechstack().equals("ReactJS16")){
				
				kbsService.editAndUpdateKbsConfigFiles(repo, projectDetails);
				//kbsThread.getDeploymentUrl("project"+projectDetails.getGitProjectId(),projectDetails.getProjectDetailId());
				
			}else{
				this.editAndUpdatePortNumber(port, repo, projectDetails);
			}
			
		}

	}
	
	void editAndUpdatePortNumber(int port, CreateRepoResponseBean repo, ProjectDetails projectDetails) {
		if (projectDetails.getTechstack().equals(TechstackEnum.JAVA_SPRINGBOOT.getValue())) {
			portNumberService.updatePortNumberInApplicationYml(repo.getId(), projectDetails.getProjectDetailId(), port);
		}
		portNumberService.updatePortNumberInGitlabYml(repo.getId(), projectDetails.getProjectDetailId(), port, projectDetails.getTechstack());
		portNumberService.addPortNumber(Integer.toString(port), projectDetails.getProjectDetailId());
	}
	
	CreateRepoResponseBean createRepoForUser(ProjectDetails projectDetails, UserDetail userDetail, int port) {
		LOG.info("Inside createRepoForUser method");
		LOG.info("Projectdetails :"+ projectDetails.toString());
		LOG.info("UserDetail : "+ userDetail.toString());
		SeedProject seedProject = seedRepo.findByTechnology(projectDetails.getTechstack()).get(0);
		CretaeRepoRequestBean request = new CretaeRepoRequestBean(userDetail.getGitUserId(), projectDetails.getProjectName(), seedProject.getCloneUrl(), RepoVisibilityLevel.PUBLIC.getValue());
		CreateRepoResponseBean response = GitFunctions.createRepoForUser(request, urlBuilder.createRepoForUserUrl(userDetail.getGitUserId()), urlBuilder.getHeader());
		projectDetails.setGitProjectId(response.getId());
		projectDetails.setStatus(StatusEnum.REPO_CREATED.getValue());
		projectDetails.setUpdatedBy(EnumList.SYSTEM.getValue());
		projectDetails.setUpdatedTime(UniqueDateFormatter.formatDate(new Date()));
		projectDetails.setProjectUrl(response.getWeb_url());
		/*if(!projectDetails.getTechstack().equals("Java8_SpringBoot2.0.5") &&
				!projectDetails.getTechstack().equals("Angular7") &&
				!projectDetails.getTechstack().equals("Angular5")){
			projectDetails.setAppAccessUrl(EnumList.APP_ACCESS_URL_IP.getValue().replace("PORT", Integer.toString(port)));
		}*/
		projectDetails.setMembersCount(1);
		LOG.info("createRepoForUser SUCCESS. Data to be saved to DB : "+ projectDetails.toString());
		projectRepository.save(projectDetails);
		emailFunction.createRepoEmailNotify(projectDetails);
		return response;
	}

	CreateUserResponseBean createGitlabUser(ProjectDetails projectDetails){
		LOG.info("inside createGitlabUSer method");
		long timestamp = new Timestamp(System.currentTimeMillis()).getTime();
		String email = projectDetails.getRequesterEmailId();
		String emailPrefix = email.substring(0,email.indexOf('@'));
		CreateUserRequestBean createUser= new CreateUserRequestBean(email, emailPrefix.concat(Long.toString(timestamp)), emailPrefix, false, "password");
		CreateUserResponseBean user = GitFunctions.createUser(createUser, urlBuilder.createUserUrl(), urlBuilder.getHeader());
		return user;
	}
	
	UserDetail getAndUpdateUserDetails(String email) {
		LOG.info("Inside getAndUpdateUserDetails method. Email : " + email);
		UserDetail res = null;
		try {
			List<GitlabUserBean> userList = GitFunctions.searchOneUserByEmail(urlBuilder.searchUserByEmailUrl(email), urlBuilder.getHeader());
			GitlabUserBean gitlabUser = userList.get(0);
			UserDetail userDetail = userDetailsRepo.findByGitEmailId(email);
			userDetail.setGitUserId(gitlabUser.getId().toString());
			userDetail.setName(gitlabUser.getName());
			userDetail.setCreatedBy(EnumList.SELF.getValue());
			userDetail.setCreatedDate(gitlabUser.getCreated_at());
			userDetail.setUpdatedBy(EnumList.SYSTEM.getValue());
			userDetail.setUpdatedDate(UniqueDateFormatter.formatDate(new Date()));
			res = userDetailsRepo.save(userDetail);
		}
		catch (Exception e) {
			LOG.error("Error getting and updating UserDetails");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		return res;

	}

	@Override
	public List<ProjectBean> getAllProject(String emailId) {
		LOG.info("Inside getAllProject method");
		List<ProjectBean> projectList = new ArrayList<>();
		List<ProjectDetails> projects = projectRepository.findAllByStatus(StatusEnum.REPO_CREATED.getValue());
		projectList = formatProjectData(projects);
		LOG.info("Project List : " + projectList);
		
		LOG.info("Getting all my projects");
		List<ProjectBean> myProjects = this.getOneUseProjects(emailId);
		if(myProjects!= null){
			LOG.info("Removing myProjects from all projects.");
			projectList.removeAll(myProjects);
			LOG.info(myProjects.toString());
		}
		return projectList;
	}

	
	@Override
	public List<ProjectBean> getOneUseProjects(String emailId) {
		LOG.info("Inside getOneUseProjects method");
		LOG.info("Getting project details for email ID : " + emailId);
		UserDetail userDetails = userDetailsRepo.findByGitEmailId(emailId);
		if(userDetails != null){
			List<ProjectDetails> projectDetails = userDetails.getProjectDetails();
			LOG.info("ProjectDetails : " + projectDetails.toString());
			Set<Long> projectIds = new HashSet<>();
			projectDetails.forEach(project -> {
				projectIds.add(project.getProjectDetailId());
			});
			List<ProjectDetails> response = projectRepository.findAllByProjectDetailId(projectIds);
			if(response.isEmpty())
				throw new NotFoundException("No project available for given emailId : "+emailId);
			List<ProjectBean> formattedResponse = formatProjectData(response);
			LOG.info("Project Detail response : "+ formattedResponse);
			return formattedResponse;
		}else{
			return null;
		}
		
		
	}
	
	public void updateExecutionCount(ProjectDetails projectDetails){
		LOG.info("Project Detail has an existing execution count of" +projectDetails.getExecutionCount());
		projectDetails.setExecutionCount(projectDetails.getExecutionCount()+1);
		projectRepository.save(projectDetails);
	}
	
	public List<ProjectBean> formatProjectData(List<ProjectDetails> projects){
		List<ProjectBean> projectList = new ArrayList<>();
		for (ProjectDetails projectDetails : projects) {
			List<UserDetailsBean> userList = new ArrayList<>();
			if(!projectDetails.getUserDetail().isEmpty()){
				List<UserDetail> users = projectDetails.getUserDetail();
				for (UserDetail userDetail : users) {
					userList.add(new UserDetailsBean(userDetail));
				}
			}
			ProjectBean bean = new ProjectBean(projectDetails, userList);
			projectList.add(bean);
		}
		return projectList;
	}

	@Override
	public List<ProjectBean> getProjectAndUser(Long projectId, Long userId) {
		LOG.info("getProjectAndUser method invoked");
		ProjectDetails projectDetails = projectRepository.findOneByProjectDetailId(projectId);
		if(projectDetails == null){
			throw new NotFoundException("No data available for userID : " + userId + " projectId : " + projectId);
		}
		List<UserDetail> userDetail = projectDetails.getUserDetail();
		List<UserDetail> filteredUserDetail = userDetail.stream().filter(user -> user.getUserDetailId() == userId).collect(Collectors.toList());
		if(filteredUserDetail == null || filteredUserDetail.isEmpty()){
			throw new NotFoundException("No data available for userID : " + userId + " projectId : " + projectId);
		}
		projectDetails.setUserDetail(filteredUserDetail);
		List<ProjectDetails> projectDetail = Arrays.asList(projectDetails);
		List<ProjectBean> projectBean = formatProjectData(projectDetail);
		return projectBean;
	}
	
	@Override
	public List<ProjectBean> getOneProject(Long projectId) {
		LOG.info("getOneProject method invoked");
		ProjectDetails projectDetails = projectRepository.findOneByProjectDetailId(projectId);
		if(projectDetails == null){
			throw new NotFoundException("No data available for projectId : " + projectId);
		}
		
		List<ProjectDetails> projectDetail = Arrays.asList(projectDetails);
		List<ProjectBean> projectBean = formatProjectData(projectDetail);
		return projectBean;
	}

	@Override
	public List<ProjectBean> searchProject(List<String> owner, List<String> techstack, List<String> projectName) {
		LOG.info("searchProject method invoked");
		LOG.info("Owner : " + owner);
		LOG.info("Techstack: " + techstack);
		LOG.info("projectName : " + projectName);
		List<ProjectDetails> projects = searchRepo.searchData(owner, techstack, projectName);
		if(projects.isEmpty() || projects == null)
			throw new NotFoundException("No project available for given parameters");
		List<ProjectBean> projectBean = formatProjectData(projects);
		return projectBean;
		
	}
}
