package com.altimetrik.playground.gitlab.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface DeleteService {

	public List<String> deleteUsersAndProjects(MultipartFile reapExcelDataFile) throws IOException;
}
