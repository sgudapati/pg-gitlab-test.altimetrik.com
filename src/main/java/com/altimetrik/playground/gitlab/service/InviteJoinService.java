package com.altimetrik.playground.gitlab.service;

import java.util.List;

import com.altimetrik.playground.gitlab.bean.AddMemberRequestBean;
import com.altimetrik.playground.gitlab.entity.InviteJoinRequest;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.exception.AltiException;

public interface InviteJoinService {

	public List<InviteJoinRequest> inviteMember(AddMemberRequestBean request, String addedBy) throws AltiException;
	
	public void addMemberToProject();
	
	public ProjectDetails removeUserFromProject(Long projectId, Long userId);
}
