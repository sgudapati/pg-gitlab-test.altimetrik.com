/**
 * 
 */
package com.altimetrik.playground.gitlab.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altimetrik.playground.gitlab.entity.SeedProject;
import com.altimetrik.playground.gitlab.exception.NotFoundException;
import com.altimetrik.playground.gitlab.repository.SeedProjectRepository;
import com.altimetrik.playground.gitlab.service.TechStackService;

/**
 * @author nmuthusamy created on 10-Oct-2018
 */
@Service
public class TechStackServiceImpl implements TechStackService {

	private static final Logger LOG = LoggerFactory.getLogger(TechStackServiceImpl.class);

	@Autowired
	SeedProjectRepository seedProjectRepo;

	@Override
	public List<SeedProject> getAllTechnology() {
		LOG.info("getAllTechnology method invoked.");
		LOG.info("Getting list of techStacks and its interface options from the system.");
		List<SeedProject> list = seedProjectRepo.findAll();
		LOG.info("Getting list of techStacks from the system - SUCCESS");
		LOG.info("TechStack List: " + list.toString());
		if (list.isEmpty())
			throw new NotFoundException("No techstack available in the system");
		return list;

	}

}
