/**
 * 
 */
package com.altimetrik.playground.gitlab.service;

import com.altimetrik.playground.gitlab.entity.PortNumbers;

/**
 * @author nmuthusamy
 * created on 30-Oct-2018
 */
public interface PortNumberService {

	PortNumbers getLastUsedPort();
	
	void addPortNumber(String portNumber, Long projectId);
	
	public void updatePortNumberInApplicationYml(Long gitlabProjectId, Long projectRequestId, int port);
	
	public void updatePortNumberInGitlabYml(Long gitlabProjectId, Long projectRequestId, int port, String techstack);
}
