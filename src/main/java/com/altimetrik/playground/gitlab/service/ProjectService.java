/**
 * 
 */
package com.altimetrik.playground.gitlab.service;

import java.util.List;

import com.altimetrik.playground.gitlab.bean.ProjectBean;
import com.altimetrik.playground.gitlab.bean.ProjectRequestBean;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.exception.AltiException;


/**
 * @author nmuthusamy
 * created on 16-Oct-2018
 */

public interface ProjectService {

	public ProjectBean submitRequest(ProjectRequestBean request) throws AltiException;
	
	public void createProject();
	
	public List<ProjectBean> getAllProject(String emailId);
	
	public List<ProjectBean> getOneUseProjects(String emailId);
	
	public List<ProjectBean> getProjectAndUser(Long projectId, Long userId);
	
	public List<ProjectBean> getOneProject(Long projectId);

	public List<ProjectBean> searchProject(List<String> owner, List<String> techstack, List<String> projectName);

	public void createProjectFunction(ProjectDetails project);
}
