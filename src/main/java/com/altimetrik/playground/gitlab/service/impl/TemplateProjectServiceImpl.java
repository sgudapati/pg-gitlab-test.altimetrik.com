package com.altimetrik.playground.gitlab.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altimetrik.playground.gitlab.bean.GitlabProjectBean;
import com.altimetrik.playground.gitlab.config.ApplicatonProperties;
import com.altimetrik.playground.gitlab.entity.InterfaceOption;
import com.altimetrik.playground.gitlab.entity.SeedProject;
import com.altimetrik.playground.gitlab.enums.EnumList;
import com.altimetrik.playground.gitlab.repository.InterfaceOptionsRepository;
import com.altimetrik.playground.gitlab.repository.SeedProjectRepository;
import com.altimetrik.playground.gitlab.service.TemplateProjectsService;
import com.altimetrik.playground.gitlab.util.GitFunctions;
import com.altimetrik.playground.gitlab.util.UrlBuilder;

@Service
public class TemplateProjectServiceImpl implements TemplateProjectsService{
	
	private static final Logger LOG = LoggerFactory.getLogger(TemplateProjectServiceImpl.class);
	
	@Autowired
	UrlBuilder urlBuilder;
	
	@Autowired
	SeedProjectRepository seedProjectRepo;
	
	@Autowired
	InterfaceOptionsRepository interfaceRepo;
	
	@Autowired
	private ApplicatonProperties properties;

	@Override
	public void updateTemplateProjectList() {
		
		String templateProjectUserId = properties.getPlaygTemplateProjUserId();
		/**
		 * Getting list of template project from GITLAB.
		 * All the template projects are created under userId 47, which is used in API url.
		 */
		List<GitlabProjectBean> templateProjectList = GitFunctions.getTemplateProjects(urlBuilder.gitProjectListForOneUserUrl(templateProjectUserId), urlBuilder.getHeader());
		List<GitlabProjectBean> templateProjectListCopy = new ArrayList<>();
		LOG.info("Template Project List : " + templateProjectList.toString());
		/**
		 * Getting list of template projects from our Database.
		 */
		LOG.info("Getting template project from our DB");
		List<SeedProject> seedProject = seedProjectRepo.findAll();
		LOG.info("Getting template project from our DB SUCCESS");
		List<Long> availableId = seedProject.stream().map(SeedProject::getGitlabProjectId).collect(Collectors.toList());
		LOG.info("List of available Template project IDs are : "+availableId );
		GitlabProjectBean newProject = null;
		/**
		 * Filtering out only new templates created in Gitlab by comparing it with the data from our DB.
		 */
		

		for (GitlabProjectBean bean : templateProjectList) {
			if(!availableId.contains(bean.getId())){
				templateProjectListCopy.add(bean);
			}
		}
		
		if(templateProjectListCopy != null && templateProjectListCopy.size()>0){
			LOG.info("List of new template projects in Gitlab which is not available in our DB : " + templateProjectList.toString());
			newProject = templateProjectListCopy.get(0);
			LOG.info("One template project going to be inserted into our DB : " + newProject.toString());
		}
		if(newProject!=null){
			
			String name = newProject.getName();
			String s[] = name.split("-", 4);
			String technology = s[0]+s[1];
			String interfaceOption = s[2];
			InterfaceOption interfaceOpt = interfaceRepo.findOneByInterfaceType(interfaceOption);
			LOG.info("New project template has TECHNOLOGY : " + technology + " DEFAULT INTERFACE : "+interfaceOpt );
			SeedProject seed = new SeedProject();
			seed.setGitlabProjectId(newProject.getId());
			seed.setProjectName(newProject.getName());
			seed.setCloneUrl(newProject.getHttp_url_to_repo());
			seed.setTechnology(technology);
			seed.setDefaultInterface(interfaceOpt);
			seed.setInterfaceOptions(new ArrayList<InterfaceOption>(){{add(interfaceOpt);}});
			LOG.info("New template data getting inserted into DB");
			seedProjectRepo.save(seed);
		}
		
	}

}
