/**
 * 
 */
package com.altimetrik.playground.gitlab.service.impl;

import java.io.File;
import java.util.Date;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altimetrik.playground.gitlab.bean.GitCommitActionBean;
import com.altimetrik.playground.gitlab.bean.GitCommitRequestBean;
import com.altimetrik.playground.gitlab.entity.PortNumbers;
import com.altimetrik.playground.gitlab.enums.EnumList;
import com.altimetrik.playground.gitlab.exception.NotFoundException;
import com.altimetrik.playground.gitlab.repository.PortNumberRepository;
import com.altimetrik.playground.gitlab.service.PortNumberService;
import com.altimetrik.playground.gitlab.util.GitFunctions;
import com.altimetrik.playground.gitlab.util.UniqueDateFormatter;
import com.altimetrik.playground.gitlab.util.UrlBuilder;

/**
 * @author nmuthusamy
 * created on 30-Oct-2018
 */
@Service
public class PortNumberServiceImpl implements PortNumberService{

	private static final Logger LOG = LoggerFactory.getLogger(PortNumberServiceImpl.class);
	
	@Autowired
	PortNumberRepository portNumberRepo;
	
	@Autowired
	UrlBuilder urlBuilder;
	
	@Override
	public PortNumbers getLastUsedPort() {
		return portNumberRepo.findLastUsedPort();
	}

	@Override
	public void addPortNumber(String portNumber, Long projectRequestId) {
		LOG.info("Inside addPortNumber method");
		LOG.info("Port number : " + portNumber + "for projectRequestId : " + projectRequestId + " is being stored into DB");
		PortNumbers newRequest = new PortNumbers(UniqueDateFormatter.formatDate(new Date()), portNumber, projectRequestId);
		portNumberRepo.save(newRequest);
		LOG.info("Port number saved in DB successfully.");
	}
	
	public void updatePortNumberInApplicationYml(Long gitlabProjectId, Long projectRequestId, int port){
		LOG.info("Inside updatePortNumberInApplicationYml method");
		LOG.info("Updating port number for gitlab project ID : " + gitlabProjectId + " and project request ID : " + projectRequestId);
		GitCommitRequestBean request = new GitCommitRequestBean();
		GitCommitActionBean[] action = new GitCommitActionBean[1];
		
		String applicationYmlContent = EnumList.CONTENT.getValue() + Integer.toString(port);
		GitCommitActionBean actionOne = new GitCommitActionBean(EnumList.UPDATE.getValue(), EnumList.APPLICATION_YML_FILE_PATH.getValue(), applicationYmlContent);
		action[0] = actionOne;
		request.setBranch("master");
		request.setCommit_message("port updated");
		request.setId(Long.toString(gitlabProjectId));
		request.setActions(action);
		GitFunctions.gitlabCommitFile(request, urlBuilder.gitCommitUrl(Long.toString(gitlabProjectId)), urlBuilder.getHeader());
		//this.addPortNumber(Integer.toString(port), projectRequestId);
	}
	
	public void updatePortNumberInGitlabYml(Long gitlabProjectId, Long projectRequestId, int port, String techstack){
		LOG.info("Inside updatePortNumberInGitlabYml method");
		LOG.info("Updating port number for gitlab project ID : " + gitlabProjectId + " and project request ID : " + projectRequestId);
		GitCommitRequestBean request = new GitCommitRequestBean();
		GitCommitActionBean[] action = new GitCommitActionBean[1];
		
		String gitlabCICDYmlContent = this.getGitlabCiYmlContent(port, techstack);
		GitCommitActionBean actionTwo = new GitCommitActionBean(EnumList.UPDATE.getValue(), EnumList.GITLAB_CI_YML_FILE_PATH.getValue(), gitlabCICDYmlContent);
		action[0] = actionTwo;
		request.setBranch("master");
		request.setCommit_message("port updated");
		request.setId(Long.toString(gitlabProjectId));
		request.setActions(action);
		GitFunctions.gitlabCommitFile(request, urlBuilder.gitCommitUrl(Long.toString(gitlabProjectId)), urlBuilder.getHeader());
		
	}
	
	public String getGitlabCiYmlContent(int port, String techstack){
		String gitlabCICDYmlContent = null;
		try{
			String ymlContent = null;
			switch (techstack) {
			case "Java SpringBoot 2.0.5":
				ymlContent = new Scanner(new File("SpringbootGitlabCICDYml.txt")).useDelimiter("\\Z").next();
				break;
				
			case "Angular6":
				ymlContent = new Scanner(new File("AngularGitlabCICDYml.txt")).useDelimiter("\\Z").next();
				break;
			case "Angular5":
				ymlContent = new Scanner(new File("AngularGitlabCICDYml.txt")).useDelimiter("\\Z").next();
				break;

			default:
				break;
			}
			if(ymlContent == null){
				throw new NotFoundException("Cant able to find yml content ");
			}
			gitlabCICDYmlContent = ymlContent.replaceAll("UPDATE_PORT_NUMBER_KEY", Integer.toString(port));
		}
		catch (Exception e) {
			LOG.error("ERROR updating port number in .gitlab-ci.yml file");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		
		return gitlabCICDYmlContent;
		
	}

}
