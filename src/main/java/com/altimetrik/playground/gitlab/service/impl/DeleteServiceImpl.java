package com.altimetrik.playground.gitlab.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.entity.UserDetail;
import com.altimetrik.playground.gitlab.enums.StatusEnum;
import com.altimetrik.playground.gitlab.repository.ProjectRepository;
import com.altimetrik.playground.gitlab.repository.UserdetailsRepository;
import com.altimetrik.playground.gitlab.service.DeleteService;
import com.altimetrik.playground.gitlab.util.GitFunctions;
import com.altimetrik.playground.gitlab.util.UniqueDateFormatter;
import com.altimetrik.playground.gitlab.util.UrlBuilder;

@Service
public class DeleteServiceImpl implements DeleteService{
	
	private static final Logger LOG = LoggerFactory.getLogger(DeleteServiceImpl.class);
	
	@Autowired
	UserdetailsRepository userDetaisRepo;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	UrlBuilder urlBuilder;

	@Override
	public List<String> deleteUsersAndProjects(MultipartFile readExcelDataFile) throws IOException {
		LOG.info("deleteUsersAndProjects method invoked.");
		List<String> gitEmailId = new ArrayList<String>();
		XSSFWorkbook workbook = new XSSFWorkbook(readExcelDataFile.getInputStream());
		XSSFSheet worksheet = workbook.getSheetAt(0);
		for (int i = 0; i < worksheet.getPhysicalNumberOfRows(); i++) {
			XSSFRow row = worksheet.getRow(i);
			System.out.println(row.getCell(0).getStringCellValue());
			gitEmailId.add(row.getCell(0).getStringCellValue());

		}
		LOG.info("List of email ids whose account and project needs to be deleted : " + gitEmailId);
		List<UserDetail> userDetails = userDetaisRepo.findByGitEmailIdIn(gitEmailId);
		
		userDetails.forEach(user->{
			
			String res = GitFunctions.deleteUser(urlBuilder.deleteUserUrl(Integer.parseInt(user.getGitUserId())), urlBuilder.getHeader());
			
			if(res.equals("SUCCESS")) {
				List<ProjectDetails> projectDetails = projectRepository.findAllByRequesterEmailId(user.getGitEmailId());
				projectDetails.forEach(project-> {
					project.setStatus(StatusEnum.DELETED.getValue());
					project.setUpdatedTime(UniqueDateFormatter.formatDate(new Date()));
					project.setUpdatedBy("ADMIN");
				});
				LOG.info("Project status getting updated...");
				projectRepository.saveAll(projectDetails);
				user.setStatus(StatusEnum.DELETED.getValue());
				user.setUpdatedDate(UniqueDateFormatter.formatDate(new Date()));
				user.setUpdatedBy("ADMIN");
				LOG.info("User statsu getting updated...");
				userDetaisRepo.save(user);
			}
			
			
		});
		
		System.out.println(gitEmailId);
		return gitEmailId;
	}

}
