package com.altimetrik.playground.gitlab.service;

import com.altimetrik.playground.gitlab.bean.UserDetailsBean;

public interface UserService {

	UserDetailsBean getUserByGitEmailId(String emailId);
}
