package com.altimetrik.playground.gitlab.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.altimetrik.playground.gitlab.bean.UserDetailsBean;
import com.altimetrik.playground.gitlab.entity.UserDetail;
import com.altimetrik.playground.gitlab.repository.UserdetailsRepository;
import com.altimetrik.playground.gitlab.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserdetailsRepository userRepo;

	@Override
	public UserDetailsBean getUserByGitEmailId(String emailId) {

		LOG.info("Inside getUserByGitEmailId method");
		LOG.info("Getting gitlab user details for email ID : "+ emailId);
		UserDetail userDetails = userRepo.findByGitEmailId(emailId);
		LOG.info("Getting gitlab user details SUCCESS...");
		if(userDetails != null){
			LOG.info("User details : " + userDetails.toString());
			return new UserDetailsBean(userDetails.getUserDetailId(), userDetails.getGitEmailId(), 
					userDetails.getName(), userDetails.getGitUsername(),userDetails.getGitPassword(), userDetails.getEmail());
			
		}else{
			LOG.info("email ID doesnt have any account in gitlab");
			return null;
		}
		
	}

}
