package com.altimetrik.playground.gitlab.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.altimetrik.playground.gitlab.bean.AddMemberRequestBean;
import com.altimetrik.playground.gitlab.bean.CreateUserRequestBean;
import com.altimetrik.playground.gitlab.bean.CreateUserResponseBean;
import com.altimetrik.playground.gitlab.bean.GitlabAddMemberRequestBean;
import com.altimetrik.playground.gitlab.bean.GitlabUserBean;
import com.altimetrik.playground.gitlab.bean.MemberBean;
import com.altimetrik.playground.gitlab.email.EmailFunction;
import com.altimetrik.playground.gitlab.entity.InviteJoinRequest;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.entity.UserDetail;
import com.altimetrik.playground.gitlab.enums.AccessLevel;
import com.altimetrik.playground.gitlab.enums.EnumList;
import com.altimetrik.playground.gitlab.enums.StatusEnum;
import com.altimetrik.playground.gitlab.exception.AltiException;
import com.altimetrik.playground.gitlab.repository.InviteJoinRequestRepository;
import com.altimetrik.playground.gitlab.repository.ProjectRepository;
import com.altimetrik.playground.gitlab.repository.UserdetailsRepository;
import com.altimetrik.playground.gitlab.service.InviteJoinService;
import com.altimetrik.playground.gitlab.util.GitFunctions;
import com.altimetrik.playground.gitlab.util.UniqueDateFormatter;
import com.altimetrik.playground.gitlab.util.UrlBuilder;

@Service
public class InviteJoinServiceImpl implements InviteJoinService{

	@Autowired
	InviteJoinRequestRepository inviteJoinRepo;
	
	@Autowired
	UrlBuilder urlBuilder;
	
	@Autowired
	UserdetailsRepository userRepo;
	
	@Autowired
	ProjectRepository projectRepo;
	
	@Autowired
	EmailFunction emailFunction;
	
	/*@Autowired
	EclipsecheService eclipsecheService;*/
	
	
	private static final Logger LOG = LoggerFactory.getLogger(InviteJoinServiceImpl.class);
	
	@Override
	public List<InviteJoinRequest> inviteMember(AddMemberRequestBean request, String addedBy) throws AltiException {
		
		LOG.info("Inside inviteMember method");
		LOG.info("Reruest body : "+ request.toString());
		
		HashSet<MemberBean> members = request.getMemberSet();
		LOG.info("Getting the no of members from the project and checking for MAX LIMIT");
		ProjectDetails projectDetails = projectRepo.findOneByProjectDetailId(request.getProjectDetailId());
		int count = projectDetails.getMembersCount() + members.size();
		if(count > Integer.parseInt(EnumList.PROJECT_MAX_MEMBER.getValue())){
			LOG.info("No of request to project exceeds the limit");
			throw new AltiException("No of members to project exceeds the limit");
		}else{
			LOG.info("Updating the member count for the project");
			projectDetails.setMembersCount(count);
			projectRepo.save(projectDetails);
		}
		
		ArrayList<InviteJoinRequest> inviteJoinRequest = new ArrayList<>();
		String date = UniqueDateFormatter.formatDate(new Date());
		members.forEach(member ->{
			InviteJoinRequest req = new InviteJoinRequest(request.getProjectDetailId(), addedBy, "NEW", member.getMemberEmailId(), member.getMemberRole(), date);  
			inviteJoinRequest.add(req);
		});
		LOG.info("Saving all the members to the DB");
		return (ArrayList<InviteJoinRequest>) inviteJoinRepo.saveAll(inviteJoinRequest);
	}

	@Override
	@Scheduled(cron = "${cron.pattern.addMemberToProject}")
	public void addMemberToProject() {
		LOG.info("addMemberToProject corn job started");
		InviteJoinRequest request = inviteJoinRepo.findOneByStatus(StatusEnum.NEW.getValue());
		UserDetail userDetail = null;
		if(request!=null){
			LOG.info("NEW unprocessed job available");
			LOG.info("Updating the execution count by 1.");
			request.setExecutionCount(request.getExecutionCount()+1);
			inviteJoinRepo.save(request);
			LOG.info("Execution count updated successfully.");
			LOG.info("Getting the project details from DB for id : " + request.getProjectDetailId());
			ProjectDetails projectDetails = projectRepo.findOneByProjectDetailId(request.getProjectDetailId());
			LOG.info("Project Details : " + projectDetails.toString());
			LOG.info("Finding gitlab user with email ID : " + request.getMemberEmailId());
			List<GitlabUserBean> gitlabUsers = GitFunctions.searchOneUserByEmail(urlBuilder.searchUserByEmailUrl(request.getMemberEmailId()), urlBuilder.getHeader());
			if(!gitlabUsers.isEmpty()){
				LOG.info("User exists in gitlab with emailId : "+ request.getMemberEmailId());
				GitlabUserBean gitlabUser = gitlabUsers.get(0);
				LOG.info("Getting user data from DB for email : " + request.getMemberEmailId());
				userDetail = userRepo.findByGitEmailId(request.getMemberEmailId());
				if(userDetail != null){
					LOG.info("User exists in local DB with email ID : " + request.getMemberEmailId());
					GitlabAddMemberRequestBean bean = new GitlabAddMemberRequestBean(projectDetails.getGitProjectId(), gitlabUser.getId(), AccessLevel.DEVELOPER.getValue());
					GitFunctions.addMemberToProject(bean, urlBuilder.addMemberToProjectUrl(projectDetails.getGitProjectId()), urlBuilder.getHeader());
				}else{
					LOG.info("User doesnt exists in local DB with email ID : " + request.getMemberEmailId() + ". Adding to DB");
					UserDetail user = new UserDetail(EnumList.SELF.getValue(), gitlabUser.getCreated_at(), gitlabUser.getEmail(), Long.toString(gitlabUser.getId()), gitlabUser.getName(), gitlabUser.getUsername());
					userDetail = userRepo.save(user);
					GitlabAddMemberRequestBean bean = new GitlabAddMemberRequestBean(projectDetails.getGitProjectId(), gitlabUser.getId(), AccessLevel.DEVELOPER.getValue());
					GitFunctions.addMemberToProject(bean, urlBuilder.addMemberToProjectUrl(projectDetails.getGitProjectId()), urlBuilder.getHeader());
				}
				
				this.updateData(request, projectDetails, userDetail);
				
			}else{
				LOG.info("User doesnt exists in gitlab with emailId : "+ request.getMemberEmailId() + ". creating new user");
				String email = request.getMemberEmailId();
				long timestamp = new Timestamp(System.currentTimeMillis()).getTime();
				CreateUserRequestBean createUser= new CreateUserRequestBean(email, email.substring(0,email.indexOf('@'))+timestamp, email.substring(0,email.indexOf('@')), false, "password");
				CreateUserResponseBean userBean = GitFunctions.createUser(createUser, urlBuilder.createUserUrl(), urlBuilder.getHeader());
				
				UserDetail user = new UserDetail(EnumList.SYSTEM.getValue(), userBean.getCreated_at(), userBean.getEmail(), userBean.getId(), userBean.getName(), userBean.getUsername(),"password");
				userDetail = userRepo.save(user);
				GitlabAddMemberRequestBean bean = new GitlabAddMemberRequestBean(projectDetails.getGitProjectId(), Long.parseLong(userDetail.getGitUserId()), AccessLevel.DEVELOPER.getValue());
				GitFunctions.addMemberToProject(bean, urlBuilder.addMemberToProjectUrl(projectDetails.getGitProjectId()), urlBuilder.getHeader());
				
				this.updateData(request, projectDetails, userDetail);
				
			}
			emailFunction.addMemberToProjectEmailNotify(projectDetails,request, userDetail);
			
			/*EclipsecheRequestBean eclipseCheRequest = new EclipsecheRequestBean(userDetail.getGitEmailId(), 
					userDetail.getGitPassword(), projectDetails.getProjectUrl(), userDetail.getGitUsername(),
					userDetail.getGitUsername()+"workspace");
			eclipsecheService.createEclipsecheWorkspace(eclipseCheRequest, projectDetails.getProjectDetailId(), 
					userDetail.getUserDetailId());*/
		}
	}
	
	public void updateData(InviteJoinRequest request,ProjectDetails projectDetails,UserDetail userDetail){
		LOG.info("Updating the batch job data into DB");
		request.setStatus(StatusEnum.MEMBER_ADDED.getValue());
		request.setUpdatedOn(UniqueDateFormatter.formatDate(new Date()));
		inviteJoinRepo.save(request);
		LOG.info("Invite Join data updated");
		LOG.info("Project details : " + projectDetails.toString());
		LOG.info("Userdetails : " + userDetail.toString());
		projectDetails.getUserDetail().add(userDetail);
		LOG.info("Updated project details : " + projectDetails.toString());
		//projectDetails.setMembersCount(projectDetails.getMembersCount()+1);
		projectRepo.save(projectDetails);
		LOG.info("Project Data updated");
	}

	@Override
	public ProjectDetails removeUserFromProject(Long projectId, Long userId) {
		LOG.info("Inside removeUserFromProject method");
		LOG.info("Removing userId : " + userId + " from projectId : " + projectId);
		ProjectDetails projectDetails = projectRepo.findOneByProjectDetailId(projectId);
		UserDetail userDetails = userRepo.findOneByUserDetailId(userId);
		GitFunctions.removeMemberFromProject(urlBuilder.removeMemberFromProjectUrl(Long.toString(projectDetails.getGitProjectId()), userDetails.getGitUserId()), urlBuilder.getHeader());
		LOG.info("Removing user project mapping from our DB.");
		projectDetails.getUserDetail().remove(userDetails);
		ProjectDetails response = projectRepo.save(projectDetails);
		return response;
	}

	
}
 
