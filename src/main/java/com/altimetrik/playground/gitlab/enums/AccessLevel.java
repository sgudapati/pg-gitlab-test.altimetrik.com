package com.altimetrik.playground.gitlab.enums;



/**
 * @author nmuthusamy
 *
 */
public enum AccessLevel {

	GUEST(10), REPORTER(20), DEVELOPER(30), MASTER(40), OWNER(50);
	
	private int value;
	 
	AccessLevel(int level) {
        this.value = level;
    }
 
    public int getValue() {
        return value;
    }
}
