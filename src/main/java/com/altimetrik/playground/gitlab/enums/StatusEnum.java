package com.altimetrik.playground.gitlab.enums;
/**
 * 
 */

/**
 * @author nmuthusamy
 * created on 16-Oct-2018
 */
public enum StatusEnum {

	NEW("NEW"),
	REPO_CREATED("REPO_CREATED"),
	MEMBER_ADDED("MEMBER_ADDED"),
	DELETED("DELETED");
	
	
	
	private String value;

	StatusEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
