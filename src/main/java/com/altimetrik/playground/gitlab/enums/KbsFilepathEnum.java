package com.altimetrik.playground.gitlab.enums;

public enum KbsFilepathEnum {

	TEST_CONNECTION_FILE_PATH	("helm/service/templates/tests/test-connection.yaml"),
	HELPERS_TPL_FILE_PATH		("helm/service/templates/_helpers.tpl"),
	DEPLOYMENT_FILE_PATH		("helm/service/templates/deployment.yaml"),
	INGRESS_FILE_PATH			("helm/service/templates/ingress.yaml"),
	NOTES_FILE_PATH				("helm/service/templates/NOTES.txt"),
	SERVICE_FILE_PATH			("helm/service/templates/service.yaml"),
	CHART_YML_FILE_PATH			("helm/service/Chart.yaml"),
	VALUES_FILE_PATH			("helm/service/values.yaml"),
	DEFAULT_FILE_PATH			("nginx/default.conf"),
	DOCKERFILE_FILE_PATH		("Dockerfile"),
	GITLAB_CI_FILE_PATH			(".gitlab-ci.yml"),
	PACKAGE_FILE_PATH			("package.json"),
	;
	
	private String value;

	KbsFilepathEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}

