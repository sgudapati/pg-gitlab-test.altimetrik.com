/**
 * 
 */
package com.altimetrik.playground.gitlab.enums;

/**
 * @author nmuthusamy
 * created on 23-Oct-2018
 */
public enum RepoVisibilityLevel {


	PUBLIC("public");
	
	private String value;
	 
	RepoVisibilityLevel(String level) {
        this.value = level;
    }
 
    public String getValue() {
        return value;
    }
    
}
