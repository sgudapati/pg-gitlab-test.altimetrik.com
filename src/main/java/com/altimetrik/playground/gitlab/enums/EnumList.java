package com.altimetrik.playground.gitlab.enums;
/**
 * @author nmuthusamy
 * created on 16-Oct-2018
 */
public enum EnumList {

	SYSTEM("SYSTEM"),
	SELF("SELF"),
	OWNER("OWNER"),
	REPO_CREATE_EMAIL_NOTIFICATION("29"),
	MEMBER_ADDED_EMAIL_NOTIFICATION("30"),
	BRANCH_MASTER("master"),
	CONTENT("server:\n  port: "),
	COMMIT_MESSAGE("port number updated"),
	APPLICATION_YML_FILE_PATH("src/main/resources/application.yml"),
	GITLAB_CI_YML_FILE_PATH(".gitlab-ci.yml"),
	UPDATE("update"),
	ZERO("0"),
	APP_ACCESS_URL_IP("http://34.216.213.156:PORT/hello/"),
	PLAYG_ENG_ENV_TEMPLATE_USER_ID("47"),
	PROJECT_MAX_MEMBER("8"),
	PASSWORD_SUFFIX("@123"),
	SERVICE_NAME("project"),
	DELETE_SERVICE_COMMAND("sh /platform/delete-service.sh "),
	GITLAB_DEFAULT_PASSWORD("password")
	;
	
	private String value;

	EnumList(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
