package com.altimetrik.playground.gitlab.enums;

/**
 * @author nmuthusamy
 *
 */
public enum UrlAppenders
{
	
    USERS("/users"),
    USER("/user"),
    PROJECTS("/projects"),
    MEMBERS("/members"),
    FILES("/files"),
    REPOSITORY("/repository"),
    COMMITS("/commits"),
    SEARCH_PROP("search="),
    QUESTION_MARK("?"),
    BACK_SLASH("/")
    ;
 
    private String value;
 
    UrlAppenders(String envUrl) {
        this.value = envUrl;
    }
 
    public String getValue() {
        return value;
    }
    
   
}