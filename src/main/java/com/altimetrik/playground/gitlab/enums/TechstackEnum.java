package com.altimetrik.playground.gitlab.enums;

public enum TechstackEnum {

	JAVA_SPRINGBOOT("Java SpringBoot 2.0.5");
	
	private String value;
	 
	TechstackEnum(String level) {
        this.value = level;
    }
 
    public String getValue() {
        return value;
    }
}
