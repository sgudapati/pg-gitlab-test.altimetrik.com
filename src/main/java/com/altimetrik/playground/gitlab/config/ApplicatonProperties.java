package com.altimetrik.playground.gitlab.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author nmuthusamy created on 09-Oct-2018
 */


@Component
@ConfigurationProperties("gitlab")
public class ApplicatonProperties {
	
	private String serverUrl;
	private String accessToken;
	private Integer projectsLimit;
	private String sonarReportUrl;
	private String playgTemplateProjUserId;
	
	public String getServerUrl() {
		return serverUrl;
	}
	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public Integer getProjectsLimit() {
		return projectsLimit;
	}
	public void setProjectsLimit(Integer projectsLimit) {
		this.projectsLimit = projectsLimit;
	}
	/**
	 * @return the sonarReportUrl
	 */
	public String getSonarReportUrl() {
		return sonarReportUrl;
	}
	/**
	 * @param sonarReportUrl the sonarReportUrl to set
	 */
	public void setSonarReportUrl(String sonarReportUrl) {
		this.sonarReportUrl = sonarReportUrl;
	}
	public String getPlaygTemplateProjUserId() {
		return playgTemplateProjUserId;
	}
	public void setPlaygTemplateProjUserId(String playgTemplateProjUserId) {
		this.playgTemplateProjUserId = playgTemplateProjUserId;
	}
	
	
}
