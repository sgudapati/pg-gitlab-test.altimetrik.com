/**
 * 
 */
package com.altimetrik.playground.gitlab.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.altimetrik.playground.gitlab.config.ApplicatonProperties;
import com.altimetrik.playground.gitlab.enums.UrlAppenders;

/**
 * @author nmuthusamy
 * created on 22-Oct-2018
 */
@Component
public class UrlBuilder {
	
	@Autowired
	private ApplicatonProperties properties;
	StringBuilder urlBuilder;

	public String searchUserByEmailUrl(String email){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.USERS.getValue());
		urlBuilder.append(UrlAppenders.QUESTION_MARK.getValue());
		urlBuilder.append(UrlAppenders.SEARCH_PROP.getValue());
		urlBuilder.append(email);
		return urlBuilder.toString();
	}
	
	public String createRepoForUserUrl(String gitUserId){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.PROJECTS.getValue());
		urlBuilder.append(UrlAppenders.USER.getValue());
		urlBuilder.append(UrlAppenders.BACK_SLASH.getValue());
		urlBuilder.append(gitUserId);
		return urlBuilder.toString();
	}
	
	public String createUserUrl(){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.USERS.getValue());
		return urlBuilder.toString();
	}
	
	public String editFileInGitLabUrl(Long gitProjectId, String filePath){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.PROJECTS.getValue());
		urlBuilder.append('/'+Long.toString(gitProjectId));
		urlBuilder.append(UrlAppenders.REPOSITORY.getValue());
		urlBuilder.append(UrlAppenders.FILES.getValue());
		urlBuilder.append('/'+filePath);
		return urlBuilder.toString();
	}
	
	public String gitCommitUrl(String projectId){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.PROJECTS.getValue());
		urlBuilder.append('/'+projectId);
		urlBuilder.append(UrlAppenders.REPOSITORY.getValue());
		urlBuilder.append(UrlAppenders.COMMITS.getValue());
		return urlBuilder.toString();
	}
	
	public String gitProjectListForOneUserUrl(String templateProjectUserId){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.USERS.getValue());
		urlBuilder.append('/'+templateProjectUserId);
		urlBuilder.append(UrlAppenders.PROJECTS.getValue());
		return urlBuilder.toString();
	}
	
	public String addMemberToProjectUrl(long gitProjectId){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.PROJECTS.getValue());
		urlBuilder.append('/'+ Long.toString(gitProjectId));
		urlBuilder.append(UrlAppenders.MEMBERS.getValue());
		return urlBuilder.toString();
	}
	
	public String removeMemberFromProjectUrl(String gitProjectId, String gitUserId){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.PROJECTS.getValue());
		urlBuilder.append('/'+ gitProjectId);
		urlBuilder.append(UrlAppenders.MEMBERS.getValue());
		urlBuilder.append('/'+ gitUserId);
		return urlBuilder.toString();
	}
	
	public String getCommitListtUrl(String gitProjectId){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.PROJECTS.getValue());
		urlBuilder.append('/'+ gitProjectId);
		urlBuilder.append(UrlAppenders.REPOSITORY.getValue());
		urlBuilder.append(UrlAppenders.COMMITS.getValue());
		return urlBuilder.toString();
	}
	
	public String getDeleteProjectUrl(String gitProjectId){
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.PROJECTS.getValue());
		urlBuilder.append('/'+ gitProjectId);
		
		return urlBuilder.toString();
	}
	
	public String deleteUserUrl(int id) {
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append(UrlAppenders.USERS.getValue());
		urlBuilder.append("/" + Integer.toString(id));
		return urlBuilder.toString();
	}
	
	public String getAllProjectUrl(String page) {
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append("projects");
		urlBuilder.append("?per_page=100&page="+page);
		return urlBuilder.toString();
	}
	
	public String getAllUsersUrl(String page) {
		urlBuilder = new StringBuilder(properties.getServerUrl());
		urlBuilder.append("users");
		urlBuilder.append("?per_page=100&page="+page);
		return urlBuilder.toString();
	}

	public HttpHeaders getHeader(){
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Private-Token", properties.getAccessToken());
		return headers;
		}
	
	public HttpHeaders getBasicHeader(){
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
		}


	
}
