/**
 * 
 */
package com.altimetrik.playground.gitlab.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author nmuthusamy created on 22-Oct-2018
 */
public class UniqueDateFormatter {
	
	private static final Logger LOG = LoggerFactory.getLogger(UniqueDateFormatter.class);

	public static String formatDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formatter.format(date);
	}

	public static String format(String date) {
		String formatDate = null;
		try{
			Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(date);  
		    formatDate = new SimpleDateFormat("dd MMM yyyy").format(date1).toUpperCase();
		}catch(Exception e){
			LOG.error("ERROR formatting date in format method of UniqueDateFormatter class");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		
		return formatDate;
	}
}
