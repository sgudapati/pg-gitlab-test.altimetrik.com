	/**
 * 
 */
package com.altimetrik.playground.gitlab.util;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.altimetrik.playground.gitlab.bean.CreateRepoResponseBean;
import com.altimetrik.playground.gitlab.bean.CreateUserRequestBean;
import com.altimetrik.playground.gitlab.bean.CreateUserResponseBean;
import com.altimetrik.playground.gitlab.bean.CretaeRepoRequestBean;
import com.altimetrik.playground.gitlab.bean.GitCommitRequestBean;
import com.altimetrik.playground.gitlab.bean.GitCommitResponseBean;
import com.altimetrik.playground.gitlab.bean.GitlabAddMemberRequestBean;
import com.altimetrik.playground.gitlab.bean.GitlabProjectBean;
import com.altimetrik.playground.gitlab.bean.GitlabUserBean;
import com.altimetrik.playground.gitlab.repository.UserdetailsRepository;

/**
 * @author nmuthusamy
 * created on 22-Oct-2018
 */
public class GitFunctions {

	
	
	private static final Logger LOG = LoggerFactory.getLogger(GitFunctions.class);
	
	static RestTemplate restTemplate = new RestTemplate();
	
	public static List<GitlabUserBean> searchOneUserByEmail(String gitApiUrl, HttpHeaders header){
		LOG.info("searchOneUserByEmail GitFunction method");
		LOG.info("Gitlab api endpoint : "+ gitApiUrl);
		ResponseEntity<List<GitlabUserBean>> user = null;
		List<GitlabUserBean> userBean = null;
		
		HttpEntity<?> entity = new HttpEntity<Object>(null, header);
		try{
			LOG.info("Making Rest call to gitlab api");
			user = restTemplate.exchange(gitApiUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<GitlabUserBean>>() {
				});
			LOG.info("Making Rest call to gitlab api - SUCCESS.");
		}catch(Exception e){
			LOG.error("Error searching gitlab user...");
			LOG.info(e.getLocalizedMessage());
			LOG.info(e.getMessage());
			e.printStackTrace();
		}
		
		userBean = user.getBody();
		LOG.info("Userbean : "+userBean.toString());	
		return userBean;
	}
	
	public static CreateRepoResponseBean createRepoForUser(CretaeRepoRequestBean request, String gitApiUrl, HttpHeaders header) {
		LOG.info("createRepoForUser GitFunction method");
		LOG.info("Gitlab api endpoint : "+ gitApiUrl);
		LOG.info("CretaeRepoRequestBean : "+ request.toString());
		ResponseEntity<CreateRepoResponseBean> repo = null;
		HttpEntity<?> entity = new HttpEntity<Object>(request, header);
		LOG.info("Making Rest call to gitlab api");
		try{
			repo = restTemplate.exchange(gitApiUrl, HttpMethod.POST, entity, CreateRepoResponseBean.class);
			LOG.info("Making Rest call to gitlab api - SUCCESS.");
			LOG.info("CreateRepoResponseBean : " + repo.getBody().toString());
			TimeUnit.SECONDS.sleep(3);
			
		}catch(Exception e){
			LOG.error("ERROR creating repo");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
		}
		return repo.getBody();
	}
	
	public static CreateUserResponseBean createUser(CreateUserRequestBean createUserRequestBean, String gitApiUrl, HttpHeaders header) {
		LOG.info("Inside createUser GitFunction method");
		LOG.info("Gitlab api endpoint to create new user: " + gitApiUrl);
		LOG.info("CreateUserRequestBean : " + createUserRequestBean.toString());
		ResponseEntity<CreateUserResponseBean> userDetails = null;
		HttpEntity<?> entity = new HttpEntity<Object>(createUserRequestBean, header);
		LOG.info("Making Rest call to gitlab api");
		userDetails = restTemplate.exchange(gitApiUrl, HttpMethod.POST, entity, CreateUserResponseBean.class);
		LOG.info("Making Rest call to gitlab api - SUCCESS.");
		LOG.info("CreateRepoResponseBean : " + userDetails.getBody().toString());
		return userDetails.getBody();
	}

	public static void gitlabCommitFile(GitCommitRequestBean request, String gitApiUrl, HttpHeaders header) {
		LOG.info("Inside gitlabCommitFile GitFunction method");
		LOG.info("Gitlab api endpoint to commit file: " + gitApiUrl);
		HttpEntity<?> entity = new HttpEntity<Object>(request, header);
		LOG.info("Making Rest call to gitlab api");
		try{
			restTemplate.postForEntity(gitApiUrl, entity, Object.class);
			LOG.info("Making Rest call to gitlab api - SUCCESS.");
		}catch (Exception e) {
			LOG.error("ERROR in commiting files");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			e.printStackTrace();
		}
		
	}
	
	public static List<GitlabProjectBean> getTemplateProjects(String gitApiUrl, HttpHeaders header){
		LOG.info("Inside getTemplateProjects GitFunction method");
		LOG.info("Gitlab api endpoint to getTemplateProjects: " + gitApiUrl);
		HttpEntity<?> entity = new HttpEntity<Object>(null, header);
		LOG.info("Making Rest call to gitlab api");
		ResponseEntity<List<GitlabProjectBean>> templateProject = null;
		List<GitlabProjectBean> templateProjectList = null;
		try{
			templateProject =  restTemplate.exchange(gitApiUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<GitlabProjectBean>>() {});
			templateProjectList = templateProject.getBody();
			LOG.info("Making Rest call to gitlab api -getTemplateProjects- SUCCESS.");
		}catch (Exception e) {
			LOG.error("ERROR in getTemplateProjects function");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			
		}	
		return templateProjectList;
	}
		
	public static void addMemberToProject(GitlabAddMemberRequestBean bean, String gitApiUrl, HttpHeaders header){
		LOG.info("Inside addMemberToProject GitFunction method");
		LOG.info("Gitlab api endpoint to addMemberToProject: " + gitApiUrl);
		LOG.info("Request body : " + bean.toString());
		HttpEntity<?> entity = new HttpEntity<Object>(bean, header);
		LOG.info("Making Rest call to gitlab api");
		
		try{
			restTemplate.exchange(gitApiUrl, HttpMethod.POST, entity, Object.class);
			LOG.info("Making Rest call to gitlab api -addMemberToProject- SUCCESS.");
		}catch (Exception e) {
			LOG.error("ERROR in addMemberToProject function");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			
		}	
	}
	
	public static void removeMemberFromProject(String gitApiUrl, HttpHeaders header){
		LOG.info("Inside removeMemberFromProject GitFunction method");
		LOG.info("Gitlab api endpoint to removeMemberFromProject: " + gitApiUrl);
		
		HttpEntity<?> entity = new HttpEntity<Object>(null, header);
		LOG.info("Making Rest call to gitlab api");
		
		try{
			restTemplate.exchange(gitApiUrl, HttpMethod.DELETE, entity, Object.class);
			LOG.info("Making Rest call to gitlab api -removeMemberFromProject- SUCCESS.");
		}catch (Exception e) {
			LOG.error("ERROR in removeMemberFromProject function");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			
		}	
	}
	
	public static List<GitCommitResponseBean> getCommitList(String gitApiUrl, HttpHeaders header){
		LOG.info("Inside getCommitList GitFunction method");
		LOG.info("Gitlab api endpoint to getCommitList: " + gitApiUrl);
		HttpEntity<?> entity = new HttpEntity<Object>(null, header);
		LOG.info("Making Rest call to gitlab api");
		ResponseEntity<List<GitCommitResponseBean>> commitData = null;
		List<GitCommitResponseBean> commitList = null;
		try{
			commitData =  restTemplate.exchange(gitApiUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<GitCommitResponseBean>>() {});
			commitList = commitData.getBody();
			LOG.info("Making Rest call to gitlab api -getCommitList- SUCCESS.");
		}catch (Exception e) {
			LOG.error("ERROR in getCommitList function");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			
		}	
		return commitList;
	}
	
	public static String deleteProject(String gitApiUrl, HttpHeaders header){
		HttpEntity<?> entity = new HttpEntity<Object>(null, header);
		ResponseEntity<?> res = null;
		try{
			res =  restTemplate.exchange(gitApiUrl, HttpMethod.DELETE, entity, Object.class);
			LOG.info("Making Rest call to gitlab api -deleteProject- SUCCESS.");
			if(res.getStatusCodeValue() == 202){
				return "SUCCESS";
			}
		}catch (Exception e) {
			LOG.error("ERROR in deleteProject function");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			
		}
		return "FAILURE";
	}
	
	public static String deleteUser(String gitApiUrl, HttpHeaders header){
		LOG.info("Inside deleteUser git function.");
		LOG.info("Gitlab api endpoint to deleteUser : " + gitApiUrl);
		HttpEntity<?> entity = new HttpEntity<Object>(null, header);
		ResponseEntity<?> res = null;
		try{
			res =  restTemplate.exchange(gitApiUrl, HttpMethod.DELETE, entity, Object.class);
			LOG.info("Making Rest call to gitlab api -deleteUser- SUCCESS.");
			if(res.getStatusCodeValue() == 204){
				return "SUCCESS";
			}
		}catch (Exception e) {
			LOG.error("ERROR in deleteUser function");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			
		}
		return "FAILURE";
	}
	
/*public static List<AllProjectsBean> getAllProj(String gitApiUrl, HttpHeaders header){
		
		LOG.info("Inside getAllProj GitFunction method");
		LOG.info("Gitlab api endpoint to getAllProj: " + gitApiUrl);
		HttpEntity<?> entity = new HttpEntity<Object>(null, header);
		LOG.info("Making Rest call to gitlab api");
		ResponseEntity<List<AllProjectsBean>> projectDetails = null;
		List<AllProjectsBean> projects = null;
		try{
			projectDetails =  restTemplate.exchange(gitApiUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<AllProjectsBean>>() {});
			projects = projectDetails.getBody();
			LOG.info("Making Rest call to gitlab api -getAllProj- SUCCESS.");
		}catch (Exception e) {
			LOG.error("ERROR in getAllProj function");
			LOG.info(e.getMessage());
			LOG.info(e.getLocalizedMessage());
			
		}	
		return projects;
	}*/


public static List<GitlabUserBean> getAllUsers(String allUsersUrl, HttpHeaders header) {
	LOG.info("Inside getAllUsers GitFunction method");
	LOG.info("Gitlab api endpoint to getAllUsers: " + allUsersUrl);
	HttpEntity<?> entity = new HttpEntity<Object>(null, header);
	LOG.info("Making Rest call to gitlab api");
	ResponseEntity<List<GitlabUserBean>> userDetails = null;
	List<GitlabUserBean> users = null;
	try{
		userDetails =  restTemplate.exchange(allUsersUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<GitlabUserBean>>() {});
		users = userDetails.getBody();
		LOG.info("Making Rest call to gitlab api -getAllUsers- SUCCESS.");
	}catch (Exception e) {
		LOG.error("ERROR in getAllUsers function");
		LOG.info(e.getMessage());
		LOG.info(e.getLocalizedMessage());
		
	}	
	return users;
}
	
}
