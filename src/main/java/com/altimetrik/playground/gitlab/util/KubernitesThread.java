package com.altimetrik.playground.gitlab.util;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.altimetrik.playground.gitlab.bean.RepoHostDetails;
import com.altimetrik.playground.gitlab.bean.RepoHostRequest;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.repository.ProjectRepository;

@Component
public class KubernitesThread {

	private static final Logger LOG = LoggerFactory.getLogger(KubernitesThread.class);

	static RestTemplate restTemplate = new RestTemplate();

	@Autowired
	ProjectRepository projectRepo;

	public void getDeploymentUrl(String serviceName, Long projectDetailId) {
		LOG.info("getDeploymentUrl method started for serviceName : " + serviceName + " projectDetailsId : "+ projectDetailId);
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.execute(new Runnable() {

			@Override
			public void run() {
				try {
					LOG.info("Service Name : " + serviceName);
					LOG.info("Id : " + projectDetailId);
					RepoHostDetails hostDetail = new RepoHostDetails();
					ResponseEntity<RepoHostDetails> res = null;
					String hostUrl = null;
					boolean exit = false;
					int cnt = 0;
					while( !exit && cnt < 12 ){
						LOG.info("While loop count for serviceName : " + serviceName + " is " + cnt + " Exit condition : " + exit);
						cnt++;
						RepoHostRequest req = new RepoHostRequest("sh /platform/get-loadbalancer-host.sh " + serviceName);
						HttpHeaders headers = new HttpHeaders();
						headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
						headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
						HttpEntity<?> entity = new HttpEntity<>(req, headers);
						res = restTemplate.exchange("http://10.101.102.56:1025/routes/shell/exec", HttpMethod.POST, entity,RepoHostDetails.class);
						hostDetail = res.getBody();
						LOG.info("Response : " +hostDetail.toString());
						
						if(hostDetail.getStatus().getStatusCode() == 0){
							LOG.info("Status code is 0");
							exit = false;
						}else if(hostDetail.getStatus().getStatusCode() == 1 && hostDetail.getStatus().getMessageDescription().equals("<pending>")){
							LOG.info("Status code is 1 but state : PENDING");
							exit = false;
						}else if(hostDetail.getStatus().getStatusCode() == 1 && hostDetail.getStatus().getMessageDescription() == null){
							LOG.info("Status code is 1 but state : NULL");
							exit = false;
						}else{
							exit = true;
							hostUrl = hostDetail.getStatus().getMessageDescription();
							LOG.info("Status coed is 1, hostUrl : " + hostUrl);
						}
						
						TimeUnit.SECONDS.sleep(10);
					}
					if(!hostUrl.isEmpty() || hostUrl != null ){
						ProjectDetails projectDetails = projectRepo.findOneByProjectDetailId(projectDetailId);
						LOG.info("Project details from DB : " + projectDetails.toString());
						projectDetails.setAppAccessUrl(hostUrl+"/v1/swagger-ui.html");
						LOG.info("Data to be saved to DB : " + projectDetails.toString());
						projectRepo.save(projectDetails);
					}
					
				} catch (Exception e) {
					LOG.error("ERROR getting deployment URL");
					LOG.info(e.getLocalizedMessage());
					LOG.info(e.getMessage());
					e.printStackTrace();
				}
			}
		});

		executorService.shutdown();

	}
}
