package com.altimetrik.playground.gitlab.batchjobs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.altimetrik.playground.gitlab.bean.DeleteServiceRequestBean;
import com.altimetrik.playground.gitlab.bean.DeleteServiceResponseBean;
import com.altimetrik.playground.gitlab.bean.GitCommitResponseBean;
import com.altimetrik.playground.gitlab.entity.ProjectDetails;
import com.altimetrik.playground.gitlab.enums.EnumList;
import com.altimetrik.playground.gitlab.enums.StatusEnum;
import com.altimetrik.playground.gitlab.repository.ProjectRepository;
import com.altimetrik.playground.gitlab.util.GitFunctions;
import com.altimetrik.playground.gitlab.util.UrlBuilder;

@Component
public class DeleteServiceBatch {
	
	private static final Logger LOG = LoggerFactory.getLogger(DeleteServiceBatch.class);

	@Autowired
	ProjectRepository projectRepo;
	
	@Autowired
	UrlBuilder urlBuilder;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("${playground.commitDiffHours}")
	private String commitDiffHours;
	
	@Value("${playground.deleteServiceUrl}")
	private String deleteServiceUrl;
	
	@Scheduled(cron = "${cron.pattern.deleteDeployedService}")
	public void deleteDeployedService() {
		LOG.info("deleteDeployedService corn job started");
		LOG.info("Getting list of projects from DB");
		List<ProjectDetails> projectDetails = projectRepo.findAllByStatus(StatusEnum.REPO_CREATED.getValue());
		LOG.info("Getting list of project from DB SUCCESS. No.of projects : " + projectDetails.size()+1);
		List<GitCommitResponseBean> commitResponse = null;
		int count = 1;
		for (ProjectDetails project : projectDetails) {
				LOG.info("Loop count : " + count);
				count++;
				LOG.info("Doing the delete constrain check for project  : " + project.toString());
				commitResponse = GitFunctions.getCommitList(urlBuilder.getCommitListtUrl(Long.toString(project.getGitProjectId())), urlBuilder.getHeader());
				if(commitResponse != null){
					GitCommitResponseBean lastCommit = commitResponse.get(0);
					String lastCommitDate = lastCommit.getCommitted_date();
					LOG.info("Last committed date : " + lastCommitDate);
					Date d1 = null; 
					try {
						d1=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(lastCommitDate);  
					} catch (ParseException e) {
						LOG.error("Date parse exception");
						LOG.info(e.getMessage());
						LOG.info(e.getLocalizedMessage());
					}
					Date d2 = new Date();
					long diff = d2.getTime() - d1.getTime();
					long diffHours = diff / (60 * 60 * 1000);
					LOG.info("Time difference : " + diffHours);
					
					if(diffHours>Integer.parseInt(commitDiffHours)){
						String serviceName = EnumList.SERVICE_NAME.getValue()+project.getGitProjectId();
						LOG.info("Diff > 24 : Deleting service  " + serviceName);
						DeleteServiceRequestBean request = new DeleteServiceRequestBean(EnumList.DELETE_SERVICE_COMMAND.getValue() + serviceName);
						HttpEntity<?> entity = new HttpEntity<Object>(request, urlBuilder.getBasicHeader());
						ResponseEntity<DeleteServiceResponseBean> responseBean = 
								restTemplate.exchange(deleteServiceUrl, HttpMethod.POST, entity, DeleteServiceResponseBean.class);
						DeleteServiceResponseBean response = responseBean.getBody();
						LOG.info("Delete Service Response : " + response.toString());
					}
					
				}
			
		}
	}
	
}
