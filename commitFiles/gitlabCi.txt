variables:
  RELEASE_NAME: "SERVICE_NAME"
  SERVICE_PORT: 1099
  REGISTRY_URL: 279716074232.dkr.ecr.us-east-1.amazonaws.com/pgdockrepo
  SONAR_URL: "http://pg-sonar-altimetrik-com-82028342.us-west-2.elb.amazonaws.com"
  SONAR_LOGIN: "cc3fc35cad01a325d1b3904bcf3aa38b153fa7f1"
  

stages:
  - Build
  - Test
  - ReleaseCleanup
  - ReleaseDeploy


Build:
  stage: Build
  script:
  - mvn  clean install
  - $(aws ecr get-login --no-include-email --region us-east-1)
  - docker build -t 279716074232.dkr.ecr.us-east-1.amazonaws.com/pgdockrepo:$CI_PIPELINE_ID .
  - docker push 279716074232.dkr.ecr.us-east-1.amazonaws.com/pgdockrepo:$CI_PIPELINE_ID
  
sonar:
    stage: Test
    script:
    - mvn --batch-mode verify sonar:sonar -Dsonar.exclusions="pom.xml" -Dsonar.host.url=$SONAR_URL -Dsonar.login=$SONAR_LOGIN


ReleaseCleanup:
   stage: ReleaseCleanup
   script: 
   -  echo `pwd`
   -  cd ./helm && /usr/local/bin/helm delete --purge $RELEASE_NAME && exit 0
   allow_failure: true


ReleaseDeploy:
   stage: ReleaseDeploy
   script:
   - echo `pwd`
   - sed -i s/#BUILD_ID#/$CI_PIPELINE_ID/g ./helm/service/values.yaml 
   - sed -i s/#SERVICE_PORT#/$SERVICE_PORT/g ./helm/service/values.yaml
   - cd ./helm && /usr/local/bin/helm install service --name $RELEASE_NAME