FROM openjdk:8-jre-alpine3.7

RUN mkdir -p /commitFiles
ADD /target/#ARTIFACT# //
ADD /src/main/resources/db-config.properties //
ADD /src/main/resources/application.yml //
ADD /src/main/resources/application.properties //
ADD /commitFiles/SpringbootGitlabCICDYml.txt /commitFiles/
ADD /commitFiles/AngularGitlabCICDYml.txt /commitFiles/
ADD /commitFiles/Chart.txt /commitFiles/
ADD /commitFiles/deployment.txt /commitFiles/
ADD /commitFiles/helpers.txt /commitFiles/
ADD /commitFiles/ingress.txt /commitFiles/
ADD /commitFiles/NOTES.txt /commitFiles/
ADD /commitFiles/service.txt /commitFiles/
ADD /commitFiles/test-connection.txt /commitFiles/
ADD /commitFiles/angular_default.txt /commitFiles/
ADD /commitFiles/angular_dockerfile.txt /commitFiles/
ADD /commitFiles/react_dockerfile.txt /commitFiles/
ADD /commitFiles/react_package.txt /commitFiles/
ADD /commitFiles/test.txt /commitFiles/
ADD /commitFiles/gitlabCi.txt /commitFiles/
ADD /commitFiles/gitlabCi3.txt /commitFiles/
ADD /commitFiles/js_gitlabCi.txt /commitFiles/
ADD /commitFiles/js_gitlabCi3.txt /commitFiles/
ADD /commitFiles/values.txt /commitFiles/
ADD /commitFiles/values3.txt /commitFiles/
ADD /commitFiles/js_values.txt /commitFiles/
ADD /commitFiles/js_values3.txt /commitFiles/


ENTRYPOINT ["java", "-jar", "/#ARTIFACT#"]
